CC=cc
CFLAGS=-g -Wall -Wextra -Wno-unused-function -Werror -std=c99 -D_XOPEN_SOURCE=600
LDFLAGS=-g

OBJS=buffer.o cmd.o cmd_parser.o range.o cmddef.o stralloc.o cle.o util.o linenoise.o te.o main.o
TESTOBJS=stralloc.o stralloc_test.o buffer.o buffer_test.o cmd_test.o range.o range_test.o cmddef.o te.o te_test.o cmd_parser.o cle.o linenoise.o util.o util_test.o

all: te

te: $(OBJS)
	$(CC) $(LDFLAGS) $(LIBS) -o te $(OBJS)

fmt:
	clang-format -i *.[ch]

tags: *.c
	ctags *.c

te_test: $(TESTOBJS) munit/munit.o
	$(CC) $(LDFLAGS) -o te_test munit/munit.o $(TESTOBJS)

unittest: te_test
	./te_test

playtest: te
	PATH=$$PWD:$$PATH prove -v </dev/null

check: unittest playtest

stralloc.o stralloc_test.o: stralloc.h
buffer.o buffer_test.o: buffer.h
buffer.o: te.h
cmd.o: buffer.h cmd.h cmd_parser.h cmd_parser4cmd.h range.h cmddef.h stralloc.h te.h
cmd_parser.o: cmd_parser.h cle.h
cmd_test.o: cmd.c
range.o range_test.o: buffer.h range.h te.h
cmddef.o: buffer.h range.h stralloc.h te.h util.h
cle.o: linenoise.h
te.o: buffer.h cle.h cmd.h cmd_parser.h te.h
util.o util_test.o: stralloc.h

clean:
	rm -f te te_test *.o t/*.tmp

.PHONY: all fmt unittest playtest check clean
