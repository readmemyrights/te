# te -- text editor

There are so many text editors out there, and none of their developers were uncreative
enough to call them "te". One day I changed that.

## What is it?

Te is a text editor. Unlike most text editors around today it's focused on
non-interactive use and being easy to extend. Unlike [the might ED][edpasta]
it's not line-oriented and its command language is more regular
(citation needed).

[edpasta]: https://www.gnu.org/fun/jokes/ed-msg.txt

As of writing this, most of the basic command set is implemented, it's possible
to give line addresses, insert delete and change text, and do multiple buffers,
although there's no such thing as cut and paste yet. There are many things that
need to be added, but I don't have much time to work on this, and even if I did
it's not likely this will ever be appropriate for mass-use. If you want a
feature in here best implement it yourself, and make sure C programming is
something you find fun. I do.

## Why?

Because I felt like it. There's that meme that you don't become a
**REAL PROGRAMMER** until you make your own text editor, and this is my
attempt at reaching that level.

I'd also like to experiment with various ideas and this project has proved
an interesting away to do so. My hope is that average readers of this source
code will be able to learn something from it.

## build

My goal is to get this program running on most modern unixes. However I
don't have a chance to test it on other systems so help with that would be
appreciated. Assuming it works:

```shell
make
```

Currently there's no install directive, but that's a priority. To run the
tests:

```shell
make check
```

## Contribute

Any help would be appreciated.
