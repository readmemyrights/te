#include <errno.h>
#include <fcntl.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "buffer.h"
#include "te.h"
#include "stralloc.h"

#define BUFFERS_MAX 1024

#define BUFFL_DIRTY (1 << 0)

#define BUFFER_FOREACH_INTER(buf)               \
	for(buf = buffers; buf < bufmax; buf++) \
		if(buf->sab.s == NULL)          \
			continue;               \
		else

struct buffer {
	struct stralloc sab;
	char *filename;
	unsigned int flags;
	size_t dotstart, dotend;
	struct stralloc sal;
};

static buffer_t buffers[BUFFERS_MAX];
static buffer_t *freebuf = buffers;
static buffer_t *bufmax = buffers;
static buffer_t *current_buffer = NULL;

static int buffer_lineindex(buffer_t *buf);

static buffer_t *
buffer_make(void)
{
	buffer_t *r;

	for(r = freebuf; r < &buffers[BUFFERS_MAX] && r->sab.s != NULL; r++)
		;

	if(r >= &buffers[BUFFERS_MAX]) {
		error(ERR_INTER, "no more buffers");
		return NULL;
	}

	memset(r, 0, sizeof *r);

	freebuf = r + 1;
	if(current_buffer == NULL)
		buffer_switch(r);
	if(r >= bufmax)
		bufmax = r + 1;

	return r;
}

buffer_t *
buffer_new(void)
{
	buffer_t *r;
	/* strlen("#buffer 0x") + 64/4 hex digits + null byte */
	char bufname[22];

	if((r = buffer_make()) == NULL)
		return NULL;

	if(!stralloc_init(&r->sab, 28)) {
		error(ERR_OS, "stralloc: %s", strerror(errno));
		buffer_free(r);
		return NULL;
	}
	snprintf(bufname, sizeof bufname, "#buffer %p", r);
	if(buffer_setfilename(r, bufname) != OK) {
		buffer_free(r);
		return NULL;
	}
	if(buffer_lineindex(r) != OK)
		warning("couldn't index lines");

	return r;
}

buffer_t *
buffer_string(char *str)
{
	buffer_t *r;
	size_t len;
	/* strlen("#string 0x") + (64/4) hex digits + 1 null byte */
	char fnbuf[22];

	if((r = buffer_make()) == NULL)
		return r;

	len = strlen(str);
	if(!stralloc_init(&r->sab, len)) {
		error(ERR_OS, "stralloc: %s", strerror(errno));
		buffer_free(r);
		return NULL;
	}

	/* this will 100% succeed */
	stralloc_copyb(&r->sab, str, len);
	snprintf(fnbuf, sizeof fnbuf, "string %p", str);
	if(buffer_setfilename(r, fnbuf) != OK) {
		buffer_free(r);
		return NULL;
	}
	if(buffer_lineindex(r) != OK) {
		buffer_free(r);
		return NULL;
	}

	return r;
}

buffer_t *
buffer_open(const char *filename)
{
	buffer_t *r;
	char buf[BUFSIZ];
	int fd;
	ssize_t len;

	if((r = buffer_make()) == NULL)
		return NULL;

	if((fd = open(filename, O_RDONLY)) == -1 && errno != ENOENT) {
		error(ERR_OS, "%s: %s", filename, strerror(errno));
		buffer_free(r);
		return NULL;
	}

	if(!stralloc_init(&r->sab, sizeof buf))
		goto error;

	if(fd != -1) {
		while((len = read(fd, buf, sizeof buf)) > 0)
			if(!stralloc_catb(&r->sab, buf, len))
				goto error;

		if(len == -1)
			goto error;
	}

	if(buffer_setfilename(r, (char *)filename) != OK)
		goto error;
	if(buffer_lineindex(r) != OK)
		goto error;

	if(fd != -1)
		close(fd);
	return r;

error:
	error(ERR_OS, "buffer_open: %s", strerror(errno));
	if(fd != -1)
		close(fd);
	buffer_free(r);
	return NULL;
}

buffer_t *
buffer_current(void)
{
	return current_buffer;
}

buffer_t *
buffer_switch(buffer_t *buf)
{
	if(buf == NULL)
		return current_buffer;

	buffer_t *tmp = current_buffer;
	current_buffer = buf;
	return tmp;
}

buffer_t *
buffer_find(char *name)
{
	buffer_t *buf;

	BUFFER_FOREACH_INTER(buf) {
		if(buf->filename != NULL && strcmp(name, buf->filename) == 0)
			return buf;
	}

	return NULL;
}

buffer_t *
buffer_next(buffer_t *buf)
{
	if(buf == NULL)
		buf = buffers;
	else
		buf = buf + 1;

	for(; buf < bufmax; buf++)
		if(buf->sab.s != NULL)
			return buf;

	return NULL;
}

static int
buffer_validpos(buffer_t *buf, size_t pos)
{
	if(pos <= buf->sab.n)
		return 1;

	error(ERR_OOR, "out of range");
	return 0;
}

static int
buffer_validrange(buffer_t *buf, size_t start, size_t end)
{
	if(start <= end && end <= buf->sab.n)
		return 1;

	error(ERR_OOR, "out of range");
	return 0;
}

void *
buffer_get(buffer_t *buf, size_t start, size_t end, size_t *lenp)
{
	if(!buffer_validrange(buf, start, end))
		return NULL;

	*lenp = end - start;
	return buf->sab.s + start;
}

int
buffer_read(buffer_t *buf, void *dp, size_t start, size_t end)
{
	if(!buffer_validrange(buf, start, end))
		return ERR;

	memcpy(dp, buf->sab.s + start, end - start);
	return OK;
}

int
buffer_write(buffer_t *buf, int fd, size_t start, size_t end)
{
	char d[end - start + 1];
	size_t written, length;

	if(!buffer_validrange(buf, start, end))
		return ERR;

	if(buffer_read(buf, d, start, end) != OK)
		return ERR;

	written = 0;
	length = end - start;
	while(written < length) {
		ssize_t ret = write(fd, d + written, length - written);
		if(ret == -1) {
			error(ERR_OS, "write: %s", strerror(errno));
			return ERR;
		}
		written += ret;
	}

	return OK;
}

int
buffer_save(buffer_t *buf)
{
	char *fn = buffer_filename(buf);
	int fd;

	if(fn == NULL || fn[0] == '#') {
		error(ERR_USAGE, "no filename");
		return ERR;
	}

	if((fd = open(fn, O_WRONLY | O_CREAT | O_TRUNC, 0666)) == -1) {
		error(ERR_OS, "open %s: %s", fn, strerror(errno));
		return ERR;
	}

	if(buffer_write(buf, fd, 0, buffer_length(buf)) != OK) {
		close(fd);
		return ERR;
	}

	close(fd);
	buf->flags &= ~BUFFL_DIRTY;
	return OK;
}

char *
buffer_filename(buffer_t *buf)
{
	return buf->filename;
}

int
buffer_setfilename(buffer_t *buf, char *filename)
{
	char *newfn;

	if(filename != NULL) {
		if((newfn = strdup(filename)) == NULL) {
			error(ERR_OS, "out of memory");
			return ERR;
		}
	} else {
		newfn = filename;
	}

	free(buf->filename);
	buf->filename = newfn;
	return OK;
}

int
buffer_dirty(buffer_t *buf)
{
	return buf->flags & BUFFL_DIRTY;
}

size_t
buffer_length(buffer_t *buf)
{
	return buf->sab.n;
}

#define buffer_ready(buf, n) stralloc_ready(&(buf)->sab, n)
#define buffer_readyplus(buf, n) stralloc_readyplus(&(buf)->sab, n)
/*
 * XXX: stralloc doesn't natively understand size_t arrays
 *
 * We assume that buf-.lines will be always size_t
 * and these definitions will need to change when that happens
 */
#define bufferlines_ready(buf, n) stralloc_ready(&(buf)->sal, (n) * sizeof(size_t))
#define bufferlines_readyplus(buf, n) stralloc_readyplus(&(buf)->sal, (n) * sizeof(size_t))
#define BUFFER_LINES(buf) ((buf)->sal.n / sizeof(size_t))
#define BUFFER_LINESPLUS(buf, m) ((buf)->sal.n += (m) * sizeof(size_t))
#define BUFFER_LINESMINUS(buf, m) ((buf)->sal.n -= (m) * sizeof(size_t))

int
buffer_insert(buffer_t *buf, size_t where, void *blob, size_t blobsz)
{
	size_t i, morelines, whereline;
	void *p;
	size_t *linesp = (size_t *)buf->sal.s;
	static struct stralloc newsal = {NULL, 0, 0};

	if(!buffer_validpos(buf, where))
		return ERR;

	if(buffer_posline(buf, where, &whereline) != OK) {
		warning("couldn't reindex lines");
		return ERR;
	}
	if(!stralloc_insertb(&buf->sab, where, blob, blobsz)) {
		error(ERR_OS, "stralloc_isnertb: %s", strerror(errno));
		return ERR;
	}
	buf->flags |= BUFFL_DIRTY;
	if(newsal.s == NULL) {
		if(!stralloc_init(&newsal, 4 * sizeof(*linesp))) {
			error(ERR_OS, "couldn't reindex lines: %s", strerror(errno));
			return ERR;
		}
	} else
		stralloc_zero(&newsal);
	for(p = buf->sab.s + where;;) {
		if((p = memchr(p, '\n', buf->sab.s + where + blobsz - (char *)p)) == NULL)
			break;
		p++;
		size_t start = (char *)p - buf->sab.s;
		if(!stralloc_catb(&newsal, &start, sizeof start)) {
			error(ERR_OS, "couldn't reindex lines: %s", strerror(errno));
			return ERR;
		}
	}
	if(!stralloc_insertsa(&buf->sal, (whereline + 1) * sizeof(*linesp), &newsal)) {
		error(ERR_OS, "couldn't reindex lines: %s", strerror(errno));
		return ERR;
	}
	morelines = newsal.n / sizeof(*linesp);
	linesp = (size_t *)buf->sal.s;
	for(i = whereline + morelines + 1; i < BUFFER_LINES(buf); i++)
		linesp[i] += blobsz;

	return OK;
}

int
buffer_change(buffer_t *buf, size_t start, size_t end, void *blobp, size_t blobsz)
{
	ssize_t delta;

	if(blobp == NULL || blobsz == 0)
		return buffer_delete(buf, start, end);

	if(!buffer_validrange(buf, start, end))
		return ERR;

	/* this can underflow but I'm too lazy to check for this case */
	delta = blobsz - (end - start);
	if(delta != 0) {
		if(!buffer_readyplus(buf, delta)) {
			error(ERR_OS, "out of memory");
			return ERR;
		}
		memmove(buf->sab.s + end + delta, buf->sab.s + end, buf->sab.n - end);
	}
	memmove(buf->sab.s + start, blobp, blobsz);

	buf->sab.n += delta;
	buf->flags |= BUFFL_DIRTY;
	if(buffer_lineindex(buf) != OK)
		warning("couldn't reindex lines");
	return OK;
}

int
buffer_delete(buffer_t *buf, size_t start, size_t end)
{
	size_t linestart, lineend;
	size_t *linesp = (size_t *)buf->sal.s;

	if(!buffer_validrange(buf, start, end))
		return ERR;

	/* This should always work, but better safe than sorry */
	if(buffer_posline(buf, start, &linestart) == ERR
			|| buffer_posline(buf, end, &lineend) == ERR) {
		warning("couldn't index lines");
		return OK;
	}

	memmove(buf->sab.s + start, buf->sab.s + end, buf->sab.n - end);
	buf->sab.n -= end - start;
	buf->flags |= BUFFL_DIRTY;
	if(linestart != lineend) {
		/* XXX: the multiplication could overflow */
		memmove(linesp + linestart + 1, linesp + lineend + 1,
				sizeof(*linesp) * (BUFFER_LINES(buf) - lineend));
		BUFFER_LINESMINUS(buf, lineend - linestart);
	}
	for(size_t i = linestart + 1; i < BUFFER_LINES(buf); i++)
		linesp[i] -= end - start;

	return OK;
}

size_t
buffer_getdot(buffer_t *buf, size_t *startp, size_t *endp)
{
	if(startp != NULL)
		*startp = buf->dotstart;
	if(endp != NULL)
		*endp = buf->dotend;

	return buf->dotend - buf->dotstart;
}

int
buffer_setdot(buffer_t *buf, size_t start, size_t end)
{
	if(!buffer_validrange(buf, start, end))
		return ERR;

	buf->dotstart = start;
	buf->dotend = end;

	return OK;
}

static int
buffer_lineindex(buffer_t *buf)
{
	void *p;
	size_t start;

	if(buf->sal.s == NULL) {
		if(!stralloc_init(&buf->sal, 4 * sizeof(size_t))) {
			error(ERR_OS, "stralloc: %s", strerror(errno));
			return ERR;
		}
	} else {
		stralloc_zero(&buf->sal);
	}

	/* if the buffer is empty, act like there's a single empty line */
	if(buf->sab.n == 0) {
		start = 0;
		stralloc_catb(&buf->sal, &start, sizeof start);
	}

	for(start = 0; start < buf->sab.n; start += (char *)p - (buf->sab.s + start) + 1) {
		if((p = memchr(buf->sab.s + start, '\n', buf->sab.n - start)) == NULL)
			p = buf->sab.s + buf->sab.n;

		if(!stralloc_catb(&buf->sal, &start, sizeof start)) {
			/*
			 * we can't recover from this.
			 * The previous line index was nuked
			 * */
			error(ERR_OS, "buffer_lineindex: %s", strerror(errno));
			return ERR;
		}
	}

	return OK;
}

int
buffer_getline(buffer_t *buf, size_t line, size_t *startp, size_t *endp)
{
	size_t *linesp;

	if(line >= BUFFER_LINES(buf)) {
		error(ERR_OOR, "line out of range");
		return ERR;
	}

	linesp = (size_t *)buf->sal.s;
	if(startp != NULL)
		*startp = linesp[line];
	if(endp != NULL) {
		if(line == BUFFER_LINES(buf) - 1)
			*endp = buf->sab.n;
		else
			*endp = linesp[line + 1];
	}

	return OK;
}

int
buffer_posline(buffer_t *buf, size_t pos, size_t *linep)
{
	if(!buffer_validpos(buf, pos))
		return ERR;

	/*
	 * this is meant to work like binary search
	 * instead of equality however
	 * we see if pos is between lines[i] and lines[i+1]
	 * assuming that the line index was built right
	 * this algorithm should always give the correct answer.
	 * and never exit the loop
	 */

	/*
	 * special case: pos can be buf->length (the very end)
	 * and it counts as the last line
	 */
	if(pos == buf->sab.n) {
		*linep = BUFFER_LINES(buf) - 1;
		return OK;
	}

	size_t low = 0;
	size_t high = BUFFER_LINES(buf);
	size_t *linesp = (size_t *)buf->sal.s;
	while(low != high) {
		size_t i = (low + high) / 2;
		size_t lower = linesp[i];
		size_t upper = i + 1 >= BUFFER_LINES(buf) ? buf->sab.n : linesp[i + 1];
		if(lower <= pos && pos < upper) {
			if(linep != NULL)
				*linep = i;
			return OK;
		} else if(pos < lower) {
			high = i;
		} else {
			low = i + 1;
		}
	}

	/* shouldn't be reachable, if we're hear the algorithm sucks */
	fprintf(stderr, "te: failed buffer_getpos(%p, %zu, %p)\n"
	                "low=%zu (%zu)\n"
	                "high=%zu (%zu)\n"
	                "nlines=%zu\n",
	        buf, pos, linep, low, linesp[low], high, linesp[high], BUFFER_LINES(buf));
	return ERR;
}

int
buffer_lines(buffer_t *buf)
{
	return BUFFER_LINES(buf);
}

void
buffer_free(buffer_t *buf)
{
	free(buf->filename);
	free(buf->sal.s);
	if(buf->sab.s != NULL) {
		stralloc_zero(&buf->sab);
		free(buf->sab.s);
	}

	buf->sab.s = NULL;

	if(buf < freebuf)
		freebuf = buf;

	if(buf + 1 == bufmax)
		bufmax = buf;
}
