#ifndef TE_BUFFER_H
#define TE_BUFFER_H

/* sys/types.h needs to be included */

typedef struct buffer buffer_t;

buffer_t *buffer_new(void);
buffer_t *buffer_string(char *);
buffer_t *buffer_open(const char *filename);
buffer_t *buffer_current(void);
buffer_t *buffer_switch(buffer_t *buf);
buffer_t *buffer_next(buffer_t *buf);
#define BUFFER_FOREACH(buf) for(buf = buffer_next(NULL); buf != NULL; buf = buffer_next(buf))
buffer_t *buffer_find(char *name);
void *buffer_get(buffer_t *buf, size_t start, size_t end, size_t *lenp);
int buffer_read(buffer_t *buf, void *dp, size_t start, size_t end);
int buffer_write(buffer_t *buf, int fd, size_t start, size_t end);
int buffer_save(buffer_t *buf);
char *buffer_filename(buffer_t *buffer);
int buffer_setfilename(buffer_t *buf, char *filename);
size_t buffer_length(buffer_t *buf);
int buffer_dirty(buffer_t *buf);
int buffer_insert(buffer_t *buf, size_t where, void *blob, size_t blobsz);
int buffer_change(buffer_t *buf, size_t start, size_t end, void *blobp, size_t blobsz);
int buffer_delete(buffer_t *buf, size_t start, size_t end);
size_t buffer_getdot(buffer_t *buf, size_t *startp, size_t *endp);
int buffer_setdot(buffer_t *buf, size_t start, size_t end);
int buffer_getline(buffer_t *buf, size_t line, size_t *startp, size_t *endp);
int buffer_posline(buffer_t *buf, size_t pos, size_t *linep);
int buffer_lines(buffer_t *buf);
void buffer_free(buffer_t *buffer);

#endif
