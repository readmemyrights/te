#include <errno.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>

#include "buffer.h"
#include "munit/munit.h"
#include "te.h"

static MunitResult
test_buffer_new(const MunitParameter params[], void *userdata)
{
	buffer_t *buf, *buf2, *buf3;

	(void)params;
	(void)userdata;

	munit_assert_not_null(buf = buffer_new());
	munit_assert_size(buffer_length(buf), ==, 0);
	munit_assert_false(buffer_dirty(buf));
	munit_assert_not_null(buf2 = buffer_new());
	buffer_free(buf);
	munit_assert_ptr_equal(buf, buf3 = buffer_new());
	buffer_free(buf2);
	buffer_free(buf3);
	return MUNIT_OK;
}

static MunitResult
test_buffer_string(const MunitParameter params[], void *userdata)
{
	buffer_t *buf;
	char string[64];
	size_t len, len2;
	void *ret;

	(void)params;
	(void)userdata;

	munit_rand_memory(sizeof string, (unsigned char *)string);
	string[sizeof(string) - 1] = '\0';
	len = strlen(string);

	if((buf = buffer_string(string)) == NULL)
		return MUNIT_ERROR;

	munit_assert_size(buffer_length(buf), ==, len);
	munit_assert_memory_equal(9, buffer_filename(buf), "string 0x");
	if((ret = buffer_get(buf, 0, buffer_length(buf), &len2)) == NULL) {
		buffer_free(buf);
		return MUNIT_ERROR;
	}
	munit_assert_size(len2, ==, len);
	munit_assert_memory_equal(len2, ret, string);

	buffer_free(buf);
	return MUNIT_OK;
}

static void *
setup_test_buffer_open(const MunitParameter params[], void *userdata)
{
	static char tmpfile[] = "/tmp/te_buffer_open_test.XXXXXXXX";
	char *file;
	int fd;

	(void)userdata;

	if((file = (char *)munit_parameters_get(params, "te-buffer-open-file")))
		return file;

	if((fd = mkstemp(tmpfile)) == -1)
		munit_errorf("te_test: mkstemp %s: %s", tmpfile,
		             strerror(errno));

	if(write(fd, "Hello world!\n", 13) == -1)
		munit_errorf("te_test: write %s: %s", tmpfile, strerror(errno));

	close(fd);
	return tmpfile;
}

static MunitResult
test_buffer_open(const MunitParameter params[], void *userdata)
{
	char *testfile = userdata;
	buffer_t *buf;

	(void)params;

	munit_assert_not_null(buf = buffer_open(testfile));
	munit_assert_string_equal(testfile, buffer_filename(buf));
	munit_assert_size(buffer_length(buf), ==, 13);

	buffer_free(buf);
	return MUNIT_OK;
}

static void
teardown_test_buffer_open(void *userdata)
{
	char *testfile = userdata;

	if(strncmp(testfile, "/tmp/te_buffer_open_test.", 25) == 0)
		unlink(testfile);
}

static MunitResult
test_buffer_switch(const MunitParameter params[], void *userdata)
{
	buffer_t *buf1, *buf2;

	(void)params;
	(void)userdata;

	munit_assert_null(buffer_current());
	if((buf1 = buffer_new()) == NULL)
		return MUNIT_ERROR;

	munit_assert_ptr(buffer_current(), ==, buf1);

	if((buf2 = buffer_new()) == NULL) {
		buffer_free(buf1);
		return MUNIT_ERROR;
	}
	munit_assert_ptr(buffer_current(), !=, buf2);
	munit_assert_ptr(buffer_switch(buf2), ==, buf1);
	munit_assert_ptr(buffer_current(), ==, buf2);

	buffer_free(buf1);
	buffer_free(buf2);

	return MUNIT_OK;
}

static MunitResult
test_buffer_next(const MunitParameter params[], void *userdata)
{
	buffer_t *buf, *buf1, *buf2, *buf3;
	int n;

	(void)params;
	(void)userdata;

	if((buf1 = buffer_string("hello")) == NULL
			|| (buf2 = buffer_string("there")) == NULL
			|| (buf3 = buffer_string("string")) == NULL)
		return MUNIT_ERROR;

	munit_assert_ptr_equal(buf1, buffer_next(NULL));
	munit_assert_ptr_equal(buf2, buffer_next(buf1));
	munit_assert_ptr_equal(buf3, buffer_next(buf2));
	munit_assert_ptr_equal(NULL, buffer_next(buf3));

	buffer_free(buf2);
	munit_assert_ptr_equal(buf3, buffer_next(buf1));

	n = 0;
	BUFFER_FOREACH(buf)
		n += buffer_length(buf);
	munit_assert_int(n, ==, 11);

	buffer_free(buf1);
	buffer_free(buf3);
	return MUNIT_OK;
}

static MunitResult
test_buffer_find(const MunitParameter params[], void *userdata)
{
	buffer_t *buf1, *buf2, *buf3;

	(void)params;
	(void)userdata;

	if((buf1 = buffer_new()) == NULL
			|| buffer_setfilename(buf1, "name") != OK
			|| (buf2 = buffer_new()) == NULL
			|| buffer_setfilename(buf2, "another") != OK
			|| (buf3 = buffer_new()) == NULL
			|| buffer_setfilename(buf3, "yet another") != OK)
		return MUNIT_ERROR;

	munit_assert_ptr(buffer_find("name"), ==, buf1);
	munit_assert_ptr(buffer_find("another"), ==, buf2);
	munit_assert_ptr(buffer_find("yet another"), ==, buf3);
	munit_assert_null(buffer_find("nonexist"));

	buffer_free(buf1);
	munit_assert_ptr(buffer_find("name"), !=, buf1);
	munit_assert_null(buffer_find("name"));
	munit_assert_ptr(buffer_find("another"), ==, buf2);

	buffer_free(buf2);
	munit_assert_ptr(buffer_find("yet another"), ==, buf3);

	buffer_free(buf3);
	return MUNIT_OK;
}

static MunitResult
test_buffer_get(const MunitParameter params[], void *userdata)
{
	buffer_t *buf = buffer_string("Hello world!\n");
	void *world;
	size_t n;

	(void)params;
	(void)userdata;

	if(buf == NULL)
		return MUNIT_ERROR;

	munit_assert_not_null(world = buffer_get(buf, 6, 11, &n));
	munit_assert_memory_equal(n, world, "world");
	munit_assert_null(world = buffer_get(buf, 6, 0, &n)); /* should error */

	munit_assert_null(buffer_get(buf, 13, 9, &n));
	munit_assert_null(buffer_get(buf, 10, 20, &n));

	buffer_free(buf);
	return MUNIT_OK;
}

static MunitResult
test_buffer_read(const MunitParameter params[], void *userdata)
{
	buffer_t *buf;
	char d[20];

	(void)params;
	(void)userdata;

	if((buf = buffer_string("Hello world!\n")) == NULL)
		return MUNIT_ERROR;

	munit_assert(buffer_read(buf, d, 0, 6) == OK);
	munit_assert_memory_equal(6, d, "Hello ");
	munit_assert(buffer_read(buf, d, 4, 4) == OK);
	munit_assert_char(d[0], ==, 'H'); /* didn't touch dp */
	munit_assert(buffer_read(buf, d, 9, 13) == OK);
	munit_assert_memory_equal(4, d, "ld!\n");
	munit_assert(buffer_read(buf, d, 10, 15) != OK);
	munit_assert_int(te_status, ==, ERR_OOR);
	munit_assert(buffer_read(buf, d, 13, 9) != OK);
	munit_assert_int(te_status, ==, ERR_OOR);

	buffer_free(buf);
	return MUNIT_OK;
}

static MunitResult
test_buffer_write(const MunitParameter params[], void *userdata)
{
	buffer_t *buf;
	char d[20];
	int pipefd[2];

	(void)params;
	(void)userdata;

	if((buf = buffer_string("The true fruit of Discord")) == NULL)
		return MUNIT_ERROR;

	if(pipe(pipefd) == -1)
		munit_errorf("pipe: %s", strerror(errno));

	munit_assert(buffer_write(buf, pipefd[1], 0, 14) == OK);
	munit_assert(read(pipefd[0], d, 14) == 14);
	munit_assert_memory_equal(14, d, "The true fruit");

	munit_assert(buffer_write(buf, pipefd[1], 18, 25) == OK);
	munit_assert(read(pipefd[0], d, 7) == 7);
	munit_assert_memory_equal(7, d, "Discord");

	munit_assert(buffer_write(buf, pipefd[1], 19, 26) != OK);
	/* Note: test that nothing got written to the pipe */

	/*
	 * buffer_write() relies on variable length arrays
	 * if end - start is negative behavior is undefined
	 * On my machine it just segfaults.
	 *
	 * In theory this shouldn't be a problem,
	 * since now range_eval() catches that case
	 * And any code that might rely on buffer_write()
	 * or anything else that is effected by this
	 * would error out before it has to do that
	 *
	 * I desparately need to document this code better
	 *
	 * munit_assert(buffer_write(buf, pipefd[1], 25, 19) != OK);
	 */

	close(pipefd[0]);
	close(pipefd[1]);
	buffer_free(buf);
	return MUNIT_OK;
}

static MunitResult
test_buffer_insert(const MunitParameter params[], void *userdata)
{
	buffer_t *buf;
	char d[128];

	(void)params;
	(void)userdata;

	if((buf = buffer_string("The true fruit of Discord")) == NULL)
		return MUNIT_ERROR;

	munit_assert(buffer_insert(buf, 9, "grape", 5) == OK);
	munit_assert_size(buffer_length(buf), ==, 30);
	munit_assert(buffer_dirty(buf));
	munit_assert(buffer_read(buf, d, 0, 30) == OK);
	munit_assert_memory_equal(30, d, "The true grapefruit of Discord");

	munit_assert(buffer_insert(buf, 0, "Not ", 4) == OK);
	munit_assert_size(buffer_length(buf), ==, 34);
	munit_assert(buffer_read(buf, d, 0, 34) == OK);
	munit_assert_memory_equal(34, d, "Not The true grapefruit of Discord");

	munit_assert(buffer_insert(buf, 37, "overflow", 8) == ERR);
	munit_assert_int(te_status, ==, ERR_OOR);

	buffer_free(buf);
	return MUNIT_OK;
}

static MunitResult
test_buffer_change(const MunitParameter params[], void *userdata)
{
	buffer_t *buf;
	char d[32];

	(void)params;
	(void)userdata;

	if((buf = buffer_string("the true grpaefruit of discord")) == NULL)
		return MUNIT_ERROR;

	munit_assert(buffer_change(buf, 9, 14, "grape", 5) == OK);
	munit_assert_size(buffer_length(buf), ==, 30);
	munit_assert(buffer_read(buf, d, 9, 19) == OK);
	munit_assert_memory_equal(10, d, "grapefruit");

	munit_assert(buffer_change(buf, 9, 14, NULL, 0) == OK);
	munit_assert_size(buffer_length(buf), ==, 25);
	munit_assert(buffer_read(buf, d, 9, 19) == OK);
	munit_assert_memory_equal(10, d, "fruit of d");

	munit_assert(buffer_change(buf, 9, 14, "vegetable", 9) == OK);
	munit_assert_size(buffer_length(buf), ==, 29);
	munit_assert(buffer_read(buf, d, 9, 24) == OK);
	munit_assert_memory_equal(15, d, "vegetable of di");

	munit_assert(buffer_change(buf, 9, 18, "fruit", 5) == OK);
	munit_assert_size(buffer_length(buf), ==, 25);
	munit_assert(buffer_read(buf, d, 9, 19) == OK);
	munit_assert_memory_equal(10, d, "fruit of d");

	munit_assert(buffer_change(buf, 20, 30, "foxtrot", 7) == ERR);
	munit_assert_int(te_status, ==, ERR_OOR);

	buffer_free(buf);
	return MUNIT_OK;
}

static MunitResult
test_buffer_delete(const MunitParameter params[], void *userdata)
{
	buffer_t *buf;
	char d[128];

	(void)params;
	(void)userdata;

	if((buf = buffer_string("The true grapefruit of Discord")) == NULL)
		return MUNIT_ERROR;

	munit_assert(buffer_delete(buf, 9, 14) == OK);
	munit_assert(buffer_dirty(buf));
	munit_assert_size(buffer_length(buf), ==, 25);
	munit_assert(buffer_read(buf, d, 0, 25) == OK);
	munit_assert_memory_equal(25, d, "The true fruit of Discord");

	buffer_free(buf);
	return MUNIT_OK;
}

static MunitResult
test_buffer_dot(const MunitParameter params[], void *userdata)
{
	buffer_t *buf;
	size_t start, end;

	(void)params;
	(void)userdata;

	if((buf = buffer_string("antiestablishmentarianism")) == NULL)
		return MUNIT_ERROR;

	munit_assert_size(buffer_getdot(buf, &start, &end), ==, 0);
	munit_assert_size(start, ==, 0);
	munit_assert_size(end, ==, 0);

	munit_assert(buffer_setdot(buf, 4, 13) == OK);
	munit_assert_size(buffer_getdot(buf, &start, &end), ==, 9);
	munit_assert_size(start, ==, 4);
	munit_assert_size(end, ==, 13);

	munit_assert(buffer_setdot(buf, 13, 29) != OK);
	munit_assert_int(te_status, ==, ERR_OOR);

	buffer_free(buf);
	return MUNIT_OK;
}

static MunitResult
test_buffer_filename(const MunitParameter params[], void *userdata)
{
	buffer_t *buf;
	char testfn[] = "test.txt";
	char *ret;

	(void)params;
	(void)userdata;

	if((buf = buffer_new()) == NULL)
		return ERR;

	munit_assert(buffer_setfilename(buf, testfn) == OK);
	munit_assert_not_null(ret = buffer_filename(buf));
	munit_assert_ptr(ret, !=, testfn);
	munit_assert_string_equal(testfn, ret);

	munit_assert(buffer_setfilename(buf, NULL) == OK);
	munit_assert_null(buffer_filename(buf));

	buffer_free(buf);
	return MUNIT_OK;
}

static MunitResult
test_buffer_getline(const MunitParameter params[], void *userdata)
{
	buffer_t *buf;
	size_t start, end;

	(void)params;
	(void)userdata;

	if((buf = buffer_string("line 1\nline 2\nline EOF")) == NULL)
		return MUNIT_ERROR;

	munit_assert_int(buffer_lines(buf), ==, 3);
	munit_assert(buffer_getline(buf, 0, &start, &end) == OK);
	munit_assert_size(start, ==, 0);
	munit_assert_size(end, ==, 7);
	munit_assert(buffer_getline(buf, 1, &start, &end) == OK);
	munit_assert_size(start, ==, 7);
	munit_assert_size(end, ==, 14);
	munit_assert(buffer_getline(buf, 2, &start, &end) == OK);
	munit_assert_size(start, ==, 14);
	munit_assert_size(end, ==, 22);

	if(buffer_insert(buf, 7, "line 1.5\n", 9) != OK)
		return MUNIT_ERROR;
	munit_assert_int(buffer_lines(buf), ==, 4);
	munit_assert(buffer_getline(buf, 1, &start, &end) == OK);
	munit_assert_size(start, ==, 7);
	munit_assert_size(end, ==, 16);
	munit_assert(buffer_getline(buf, 3, &start, &end) == OK);
	munit_assert_size(start, ==, 23);
	munit_assert_size(end, ==, 31);

	if(buffer_delete(buf, 5, 16) != OK)
		return MUNIT_ERROR;
	munit_assert_int(buffer_lines(buf), ==, 2);
	munit_assert(buffer_getline(buf, 2, &start, &end) != OK);
	munit_assert(buffer_getline(buf, 0, &start, &end) == OK);
	munit_assert_size(start, ==, 0);
	munit_assert_size(end, ==, 12);

	if(buffer_change(buf, 4, 11, "\n\n", 2) != OK)
		return MUNIT_ERROR;
	munit_assert_int(buffer_lines(buf), ==, 4);
	munit_assert(buffer_getline(buf, 0, &start, &end) == OK);
	munit_assert_size(start, ==, 0);
	munit_assert_size(end, ==, 5);
	munit_assert(buffer_getline(buf, 1, &start, &end) == OK);
	munit_assert_size(start, ==, 5);
	munit_assert_size(end, ==, 6);

	return MUNIT_OK;
}

static MunitResult
test_buffer_posline(const MunitParameter params[], void *userdata)
{
	buffer_t *buf;
	size_t line;

	(void)params;
	(void)userdata;

	if((buf = buffer_string("line 1\nline 2\nline 3")) == NULL)
		return MUNIT_ERROR;

	munit_assert(buffer_posline(buf, 0, &line) == OK);
	munit_assert_size(line, ==, 0);
	munit_assert(buffer_posline(buf, 6, &line) == OK);
	munit_assert_size(line, ==, 0);
	munit_assert(buffer_posline(buf, 10, &line) == OK);
	munit_assert_size(line, ==, 1);
	munit_assert(buffer_posline(buf, 25, &line) == ERR);
	buffer_free(buf);

	if((buf = buffer_string("single line sans newline")) == NULL)
		return MUNIT_ERROR;

	munit_assert(buffer_posline(buf, 10, &line) == OK);
	munit_assert_size(line, ==, 0);
	munit_assert(buffer_posline(buf, buffer_length(buf), &line) == OK);
	munit_assert_size(line, ==, 0);
	buffer_free(buf);

	return MUNIT_OK;
}

static MunitTest buffer_tests[] = {
	{"/new", test_buffer_new, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/string", test_buffer_string, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/open", test_buffer_open, setup_test_buffer_open,
         teardown_test_buffer_open, MUNIT_TEST_OPTION_NONE, NULL},
	{"/switch", test_buffer_switch, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/next", test_buffer_next, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/find", test_buffer_find, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/get", test_buffer_get, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/read", test_buffer_read, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/write", test_buffer_write, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/insert", test_buffer_insert, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/change", test_buffer_change, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/delete", test_buffer_delete, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/dot", test_buffer_dot, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/filename", test_buffer_filename, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/getline", test_buffer_getline, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/posline", test_buffer_posline, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
};

MunitSuite buffer_test_suite = {"/buffer", buffer_tests, NULL, 1,
                                MUNIT_SUITE_OPTION_NONE};
