#include "linenoise.h"
#include "te.h"

int
cle_init(void)
{
	return OK;
}

char *
cle_getline(char *prompt)
{
	return linenoise(prompt);
}

int
cle_deinit(void)
{
	return OK;
}
