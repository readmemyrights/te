#ifndef CLE_H
#define CLE_H

int cle_init(void);
char *cle_getline(char *prompt);
int cle_deinit(void);

#endif
