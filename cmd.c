#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "buffer.h"
#include "cmd.h"
#include "cmd_parser4cmd.h"
#include "range.h"
#include "cmddef.h"
#include "stralloc.h"
#include "te.h"

struct cmd_t {
	struct range r;
	union cmdarg ca;
	struct cmddef *cd;
};

static void
rescue(cmd_parser_t *parser)
{
	int c;

	for(;;) {
		switch(c = advance(parser)) {
		case '\n':
		case ';':
		case EOF:
			return;
		case '\\':
			advance(parser);
			break;
		}
	}
}

static int
matchchar(cmd_parser_t *parser, int ch)
{
	int c = advance(parser);

	if(c == ch)
		return c;

	unget(parser, c);
	return 0;
}

static int
matchcharset(cmd_parser_t *parser, char *charset)
{
	int c = advance(parser);

	if(strchr(charset, c) != NULL)
		return c;

	unget(parser, c);
	return 0;
}

static void
skipspaces(cmd_parser_t *parser)
{
	int c;

	for(;;) {
		switch(c = advance(parser)) {
		case ' ':
		case '\t':
			continue;
		case '\\':
			if(matchchar(parser, '\n'))
				continue;
		/* fall through */
		default:
			unget(parser, c);
			return;
		}
	}
}

static char *
readuntil(cmd_parser_t *parser, char ch)
{
	struct stralloc sa;
	int c;

	if(!stralloc_init(&sa, 14)) {
		error(ERR_OS, "out of memory");
		return NULL;
	}

	while((c = advance(parser)) != EOF && c != ch) {
		switch(c) {
		case '\\':
			if(matchchar(parser, ch))
				c = ch;
			break;
		case '\0':
			warning("NUL in input");
			c = '\x7f';
			break;
		}
		if(!stralloc_append(&sa, c)) {
			error(ERR_OS, "out of memory");
			free(sa.s);
			return NULL;
		}
	}

	return sa.s;
}

#define ISDGT(c) ((c) >= '0' && (c) <= '9')

static size_t
readnum(cmd_parser_t *parser, int digit)
{
	int c;
	size_t r = ISDGT(digit) ? digit - '0' : 0;

	if(!ISDGT(digit))
		skipspaces(parser);
	while((c = advance(parser)), ISDGT(c))
		r = r * 10 + c - '0';

	unget(parser, c);
	return r;
}

#define ISCMDNAME(c) (((c) >= 'A' && (c) <= 'Z') || ((c) >= 'a' && (c) <= 'z'))
#define ISCMDSYM(c) ((c) == '<' || (c) == '|' || (c) == '>' || (c) == '!')

static size_t
readcmdname(cmd_parser_t *parser, char *buf, size_t bufsz)
{
	size_t i;
	int c;

	skipspaces(parser);
	c = advance(parser);
	if(ISCMDSYM(c)) {
		buf[0] = c;
		buf[1] = '\0';
		return 1;
	}

	unget(parser, c);
	for(i = 0, c = advance(parser); i < bufsz - 1 && ISCMDNAME(c);
	    i++, c = advance(parser))
		buf[i] = c;

	buf[i] = '\0';
	unget(parser, c);
	return i;
}

static char *
readrocmd(cmd_parser_t *parser, size_t *lenp)
{
	struct stralloc sa;
	int c;

	if(!stralloc_init(&sa, 14)) {
		error(ERR_OS, "out of memory");
		return NULL;
	}

	while((c = advance(parser)) != '\n' && c != ';' && c != EOF) {
		if(c == '\\')
			if((c = advance(parser)) == EOF) {
				error(ERR_USAGE, "eof in escape");
				free(sa.s);
				return NULL;
			}
		if(!stralloc_append(&sa, c)) {
			error(ERR_OS, "out of memory");
			free(sa.s);
			return NULL;
		}
	}

	unget(parser, c);
	if(lenp != NULL)
		*lenp = sa.n;
	stralloc_append(&sa, '\0');
	return sa.s;
}

static char *
readmltext(cmd_parser_t *parser, size_t *lenp)
{
	struct stralloc sa;
	int c, lastnl;

	if(!stralloc_init(&sa, 28)) {
		error(ERR_OS, "out of memory");
		return NULL;
	}

	lastnl = 1; /* it got read by our caller */
	for(;;) {
		if(lastnl && matchchar(parser, '.')) {
			if(matchchar(parser, '\n')) {
				unget(parser, '\n');
				break;
			} else {
				unget(parser, '.');
			}
		}

		if((c = advance(parser)) == EOF) {
			error(ERR_USAGE, "EOF in multiline text");
			goto error;
		}

		if(!stralloc_append(&sa, c)) {
			error(ERR_OS, "out of memory");
			goto error;
		}

		lastnl = c == '\n';
	}

	if(lenp != NULL)
		*lenp = sa.n;
	return sa.s;

error:
	free(sa.s);
	return NULL;
}

static char *
readbufname(cmd_parser_t *parser)
{
	return readuntil(parser, '"');
}

static int
readaddr(cmd_parser_t *parser, struct address *ap)
{
	int c;

	skipspaces(parser);
	switch((c = advance(parser))) {
	case '.':
		ap->at = ADDRT_DOT;
		break;
	case '@':
		ap->at = ADDRT_OFFSET;
		ap->v.offset = readnum(parser, 0);
		break;
	case '0':
	case '1':
	case '2':
	case '3':
	case '4':
	case '5':
	case '6':
	case '7':
	case '8':
	case '9':
		ap->at = ADDRT_LINE;
		ap->v.line = readnum(parser, c);
		/* in the ui lines start from 1 */
		if(ap->v.line > 0)
			ap->v.line--;
		break;
	case '$':
		ap->at = ADDRT_LAST;
		break;
	default:
		ap->at = ADDRT_UNSPEC;
		unget(parser, c);
		break;
	}

	return OK;
}

static int
readrange(cmd_parser_t *parser, struct range *rp)
{
	if(matchchar(parser, '"')) {
		if((rp->bufname = readbufname(parser)) == NULL)
			return ERR;
	} else {
		rp->bufname = NULL;
	}

	if(readaddr(parser, &rp->start) != OK)
		return ERR;

	if(matchchar(parser, ','))
		return readaddr(parser, &rp->end);
	else
		rp->end.at = ADDRT_ONLY1;

	return OK;
}

cmd_t *
cmd_read(cmd_parser_t *parser)
{
	cmd_t cmd;
	cmd_t *r;
	char cmdbuf[CMDNAME_MAX];
	size_t len;

	if(matchchar(parser, EOF))
		return CMD_EOF;

	cmd_parser_start(parser);
	memset(&cmd, 0, sizeof cmd);
	if(readrange(parser, &cmd.r) != OK)
		goto error;

	readcmdname(parser, cmdbuf, sizeof cmdbuf);
	if(cmdbuf[0] == '\0') {
		/*
		 * It's safe to assume that
		 * sizeof "print" or any other normal command name
		 * is less than sizeof cmdbuf
		 *
		 * a more important things that needs to be handled is,
		 * do we want print to be the default action always?
		 */
		strcpy(cmdbuf, "print");
	}
	if((cmd.cd = find_cmddef(cmdbuf)) == NULL) {
		error(ERR_NOCMD, "%s: no such command", cmdbuf);
		goto error;
	}

	skipspaces(parser);
	switch(cmd.cd->type) {
	case CMDAT_NONE:
		break;
	case CMDAT_STR:
		if((cmd.ca.str = readrocmd(parser, &len)) == NULL)
			goto error;
		if(strlen(cmd.ca.str) != len) {
			error(ERR_USAGE, "NUL byte in string argument");
			goto error;
		}
		break;
	case CMDAT_TEXT:
		if(matchchar(parser, '\n')) {
			if((cmd.ca.blob.blobp = readmltext(parser, &cmd.ca.blob.blobsz)) == NULL)
				goto error;
		} else {
			if((cmd.ca.blob.blobp = readrocmd(parser, &cmd.ca.blob.blobsz)) == NULL)
				goto error;
		}
		break;
	default:
		error(ERR_INTER, "%d: can't handle command argument",
		      cmd.cd->type);
		goto error;
	}

	if(!matchcharset(parser, ";\n") && !matchchar(parser, -1)) {
		int unexpect = advance(parser);
		error(ERR_USAGE, "unexpected %c after command", unexpect);
		goto error;
	}

	cmd_parser_stop(parser);
	if((r = malloc(sizeof *r)) == NULL) {
		error(ERR_OS, "out of memory");
		return CMD_GARGLED;
	}

	memcpy(r, &cmd, sizeof cmd);
	return r;

error:
	rescue(parser);
	cmd_parser_stop(parser);
	return CMD_GARGLED;
}

int
cmd_perform(cmd_t *cmd)
{
	return (*cmd->cd->fn)(cmd->r, cmd->ca);
}

int
cmd_loop(cmd_parser_t *parser, int flags)
{
	int ret = OK;
	cmd_t *cmd = cmd_read(parser);
	if(cmd == CMD_EOF)
		return NOMOCMD;
	if(cmd == CMD_GARGLED)
		goto error;

	if(flags & CLO_DEBUG)
		cmd_dump(cmd);
	if(!(flags & CLO_DRYRUN) && cmd_perform(cmd) != OK)
	error:
		ret = ERR;
	else
		te_status = OK;

	if(cmd != CMD_EOF && cmd != CMD_GARGLED)
		cmd_free(cmd);

	return ret;
}

static void
dumpblob(void *blobp, size_t blobsz)
{
	char *cp = blobp;

	for(char *p = cp; p < cp + blobsz; p++) {
		if(*p == ';' || *p == ';')
			fputc('\\', stderr);
		fputc(*p, stderr);
	}
}

void
cmd_dump(cmd_t *cmd)
{
	range_dump(cmd->r);
	fputs(cmd->cd->name, stderr);
	switch(cmd->cd->type) {
	case CMDAT_NONE:
		break;
	case CMDAT_STR:
		fputs(cmd->ca.str, stderr);
		break;
	case CMDAT_TEXT:
		dumpblob(cmd->ca.blob.blobp, cmd->ca.blob.blobsz);
		break;
	default:
		fputc('?', stderr);
	}
	fputc('\n', stderr);
}

void
cmd_free(cmd_t *cmd)
{
	switch(cmd->cd->type) {
	case CMDAT_NONE:
		break;
	case CMDAT_STR:
		free(cmd->ca.str);
		break;
	case CMDAT_TEXT:
		free(cmd->ca.blob.blobp);
		break;
	default:
		warning("don't know how to free argument type %d",
		        cmd->cd->type);
		break;
	}
	free(cmd);
}
