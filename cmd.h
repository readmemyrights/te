#ifndef CMD_H
#define CMD_H

#include "cmd_parser.h"

typedef struct cmd_t cmd_t;

#define CMD_EOF (cmd_t *)NULL
#define CMD_GARGLED (cmd_t *)1

#define CLO_DEBUG 1 << 0
#define CLO_DRYRUN 1 << 1

cmd_t *cmd_read(cmd_parser_t *parser);
int cmd_perform(cmd_t *cmd);
int cmd_loop(cmd_parser_t *parser, int flags);
void cmd_dump(cmd_t *cmd);
void cmd_free(cmd_t *cmd);

#endif
