#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "cle.h"
#include "cmd.h"
#include "te.h"

#define UNGETBUFSZ 4

struct cmd_parser_t {
	char buf[128];
	char ungetbuf[UNGETBUFSZ];
	int ungetbufi;
	char *cursor;
	char *startp;
	char *endp;
	int parsingrn;
	enum {
		GMT_STR,
		GMT_FD,
		GMT_CLE,
	} gmt;
	union {
		int fd;
	} gmu;
};

cmd_parser_t *
cmd_parser_string(char *s)
{
	cmd_parser_t *r = malloc(sizeof *r);
	if(r == NULL)
		return NULL;

	r->gmt = GMT_STR;
	r->cursor = r->startp = s;
	r->endp = strchr(s, '\0');
	r->ungetbufi = 0;
	r->parsingrn = 0;

	return r;
}

cmd_parser_t *
cmd_parser_fd(int fd)
{
	cmd_parser_t *r = malloc(sizeof *r);
	if(r == NULL)
		return NULL;

	r->gmt = GMT_FD;
	r->gmu.fd = fd;
	r->cursor = r->startp = r->endp = r->buf;
	r->ungetbufi = 0;
	r->parsingrn = 0;

	return r;
}

cmd_parser_t *
cmd_parser_cle(void)
{
	cmd_parser_t *r = malloc(sizeof *r);
	if(r == NULL)
		return NULL;

	r->gmt = GMT_CLE;
	r->cursor = r->startp = r->endp = NULL;
	r->ungetbufi = 0;
	r->parsingrn = 0;

	return r;
}

void
cmd_parser_start(cmd_parser_t *parser)
{
	parser->parsingrn = 1;
}

int
advance(cmd_parser_t *parser)
{
	ssize_t ret;

	if(parser->ungetbufi > 0)
		return parser->ungetbuf[--parser->ungetbufi];

	if(parser->cursor < parser->endp)
		return *parser->cursor++;

	switch(parser->gmt) {
	case GMT_STR:
		break; /* the whole string is already there */
	case GMT_FD:
		ret = read(parser->gmu.fd, parser->buf, sizeof parser->buf);
		switch(ret) {
		case -1:
			error(ERR_OS, "read: %s", strerror(errno));
			return EOF;
		case 0:
			return EOF;
		default:
			parser->cursor = parser->buf;
			parser->endp = parser->buf + ret;
			return *parser->cursor++;
		}
	case GMT_CLE:
		free(parser->startp);
		parser->startp = parser->cursor = cle_getline(parser->parsingrn ? te_contprompt : te_prompt);
		if(parser->startp == NULL)
			return EOF;
		/*
		 * The reason we can do this insanity
		 * is because we never access the byte at parser->endp,
		 * and we never treat this as a null-terminated string
		 * (citation needed).
		 *
		 * It's necessary because linenoise strips the terminating newline,
		 * while the parser relies on it physically being there.
		 *
		 * XXX: what about non-linenoise libraries?
		 * Why can't this be done in cle.c?
		 */
		parser->endp = strchr(parser->startp, '\0') + 1;
		parser->endp[-1] = '\n';
		return *parser->cursor++;
	default:
		fprintf(stderr, "te: call brisco: unknown getmoretype %d\n", parser->gmt);
		abort();
	}

	return EOF;
}

int
unget(cmd_parser_t *parser, int c)
{
	if(c == EOF)
		return EOF;

	if(parser->ungetbufi >= UNGETBUFSZ) {
		fputs("te: call brisco: too many unget characters\n", stderr);
		abort();
	}

	return parser->ungetbuf[parser->ungetbufi++] = c;
}

void
cmd_parser_stop(cmd_parser_t *parser)
{
	parser->parsingrn = 0;
}

void
cmd_parser_free(cmd_parser_t *parser)
{
	switch(parser->gmt) {
	case GMT_STR:
	case GMT_FD:
		break;
	case GMT_CLE:
		free(parser->startp);
		break;
	default:
		fprintf(stderr, "te: call brisco: unknown gmt %d\n", parser->gmt);
		abort();
	}
	free(parser);
}
