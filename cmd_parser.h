#ifndef CMD_PARSER_H
#define CMD_PARSER_H

typedef struct cmd_parser_t cmd_parser_t;

cmd_parser_t *cmd_parser_string(char *str);
cmd_parser_t *cmd_parser_fd(int fd);
cmd_parser_t *cmd_parser_cle(void);
void cmd_parser_free(cmd_parser_t *parser);

#endif
