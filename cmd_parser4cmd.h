#ifndef CMD_PARSER4CMD_H
#define CMD_PARSER4CMD_H

/*
 * these are definitions from cmd_parser.c
 * used only in cmd.c
 *
 * Is this too much encapsulation?
 */

void cmd_parser_start(cmd_parser_t *parser);
int advance(cmd_parser_t *parser);
int unget(cmd_parser_t *parser, int c);
void cmd_parser_stop(cmd_parser_t *parser);

#endif
