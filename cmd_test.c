#include "cmd.c"
#include "munit/munit.h"

static MunitResult
test_find_cmddef(const MunitParameter params[], void *userdata)
{
	struct cmddef *printcmddef, *pcmddef;

	(void)params;
	(void)userdata;

	munit_assert_not_null(printcmddef = find_cmddef("print"));
	munit_assert_string_equal(printcmddef->name, "print");

	munit_assert_not_null(pcmddef = find_cmddef("p"));
	munit_assert_string_equal(pcmddef->name, "p");
	munit_assert_ptr_equal(pcmddef->fn, printcmddef->fn);

	return MUNIT_OK;
}

static MunitResult
test_parser_advance(const MunitParameter params[], void *userdata)
{
	cmd_parser_t *parser;

	(void)params;
	(void)userdata;

	if((parser = cmd_parser_string("hello")) == NULL)
		return MUNIT_ERROR;

	munit_assert_int(advance(parser), ==, 'h');
	munit_assert_int(advance(parser), ==, 'e');
	munit_assert_int(advance(parser), ==, 'l');
	munit_assert_int(unget(parser, 'l'), ==, 'l');
	munit_assert_int(advance(parser), ==, 'l');
	munit_assert_int(advance(parser), ==, 'l');
	munit_assert_int(advance(parser), ==, 'o');
	munit_assert_int(advance(parser), ==, EOF);

	cmd_parser_free(parser);
	return MUNIT_OK;
}

static MunitResult
test_parser_match(const MunitParameter params[], void *userdata)
{
	cmd_parser_t *parser = cmd_parser_string(".1");

	(void)params;
	(void)userdata;

	if(parser == NULL)
		return MUNIT_ERROR;

	munit_assert(matchchar(parser, '.'));
	munit_assert_false(matchchar(parser, '#'));
	munit_assert(matchcharset(parser, "0123456789"));
	munit_assert_false(matchcharset(parser, " \t\r\n"));
	munit_assert_int(advance(parser), ==, EOF);

	cmd_parser_free(parser);
	return MUNIT_OK;
}

static MunitResult
test_parser_readuntil(const MunitParameter params[], void *userdata)
{
	cmd_parser_t *parser =
		cmd_parser_string("filename\n   /path\\/to\\file");
	char *str;

	(void)params;
	(void)userdata;

	if(parser == NULL)
		return MUNIT_ERROR;

	munit_assert_not_null(str = readuntil(parser, '\n'));
	munit_assert_string_equal(str, "filename");
	free(str);

	munit_assert_not_null(str = readuntil(parser, '/'));
	munit_assert_string_equal(str, "   ");
	free(str);

	munit_assert_not_null(str = readuntil(parser, '/'));
	munit_assert_string_equal(str, "path/to\\file");
	free(str);

	cmd_parser_free(parser);
	return MUNIT_OK;
}

static MunitResult
test_parser_readnum(const MunitParameter params[], void *userdata)
{
	cmd_parser_t *parser = cmd_parser_string("123 0992 3\t13.24");

	(void)params;
	(void)userdata;

	if(parser == NULL)
		return MUNIT_ERROR;

	munit_assert_size(readnum(parser, advance(parser)), ==, 123);
	munit_assert_int(advance(parser), ==, ' ');
	munit_assert_size(readnum(parser, 0), ==, 992);
	munit_assert_int(advance(parser), ==, ' ');
	munit_assert_size(readnum(parser, advance(parser)), ==, 3);
	munit_assert_int(advance(parser), ==, '\t');
	munit_assert_size(readnum(parser, '0'), ==, 13);
	munit_assert_int(advance(parser), ==, '.');
	munit_assert_size(readnum(parser, '.'), ==, 24);
	munit_assert_int(advance(parser), ==, EOF);

	cmd_parser_free(parser);
	return MUNIT_OK;
}

static MunitResult
test_parser_skipspaces(const MunitParameter params[], void *userdata)
{
	cmd_parser_t *parser = cmd_parser_string(" \t\t \\\n  \nprint");

	(void)params;
	(void)userdata;

	if(parser == NULL)
		return MUNIT_ERROR;

	skipspaces(parser);
	munit_assert_int(advance(parser), ==, '\n');
	skipspaces(parser);
	munit_assert_int(advance(parser), ==, 'p');

	cmd_parser_free(parser);
	return MUNIT_OK;
}

static MunitResult
test_parser_readcmdname(const MunitParameter params[], void *userdata)
{
	cmd_parser_t *parser = cmd_parser_string(
			"hello "
			"<hello >hi !bang |pipe "
			"aaaabbbbccccddddeeeeffffgggghhhhij"); /* assume
	                                                        CMDNAME_MAX is
	                                                        ~32 */
	char cmdbuf[CMDNAME_MAX];

	(void)params;
	(void)userdata;

	if(parser == NULL)
		return MUNIT_ERROR;

	munit_assert_size(readcmdname(parser, cmdbuf, sizeof cmdbuf), ==, 5);
	munit_assert_string_equal(cmdbuf, "hello");
	munit_assert_int(advance(parser), ==, ' ');
	munit_assert_size(readcmdname(parser, cmdbuf, sizeof cmdbuf), ==, 1);
	munit_assert_string_equal(cmdbuf, "<");
	munit_assert_size(readcmdname(parser, cmdbuf, sizeof cmdbuf), ==, 5);
	munit_assert_string_equal(cmdbuf, "hello");
	munit_assert_size(readcmdname(parser, cmdbuf, sizeof cmdbuf), ==, 1);
	munit_assert_string_equal(cmdbuf, ">");
	munit_assert_size(readcmdname(parser, cmdbuf, sizeof cmdbuf), ==, 2);
	munit_assert_string_equal(cmdbuf, "hi");
	munit_assert_int(advance(parser), ==, ' ');
	munit_assert_size(readcmdname(parser, cmdbuf, sizeof cmdbuf), ==, 1);
	munit_assert_string_equal(cmdbuf, "!");
	munit_assert_size(readcmdname(parser, cmdbuf, sizeof cmdbuf), ==, 4);
	munit_assert_string_equal(cmdbuf, "bang");
	munit_assert_size(readcmdname(parser, cmdbuf, sizeof cmdbuf), ==, 1);
	munit_assert_string_equal(cmdbuf, "|");
	munit_assert_size(readcmdname(parser, cmdbuf, sizeof cmdbuf), ==, 4);
	munit_assert_string_equal(cmdbuf, "pipe");
	munit_assert_size(readcmdname(parser, cmdbuf, sizeof cmdbuf), ==,
	                  sizeof(cmdbuf) - 1);
	munit_assert_char(cmdbuf[sizeof(cmdbuf) - 1], ==, '\0');

	cmd_parser_free(parser);
	return MUNIT_OK;
}

static MunitResult
test_parser_readrocmd(const MunitParameter params[], void *userdata)
{
	cmd_parser_t *parser =
		cmd_parser_string("filename\n   argument\\; it sucks;\nlmao\\");
	char *str;
	size_t len;

	(void)params;
	(void)userdata;

	if(parser == NULL)
		return MUNIT_ERROR;

	munit_assert_not_null(str = readrocmd(parser, &len));
	munit_assert_size(len, ==, 8);
	munit_assert_string_equal(str, "filename");
	free(str);
	munit_assert_int(advance(parser), ==, '\n');

	munit_assert_not_null(str = readrocmd(parser, &len));
	munit_assert_size(len, ==, 21);
	munit_assert_string_equal(str, "   argument; it sucks");
	free(str);
	munit_assert_int(advance(parser), ==, ';');

	munit_assert_not_null(str = readrocmd(parser, &len));
	munit_assert_size(len, ==, 0);
	munit_assert_string_equal(str, "");
	free(str);
	munit_assert_int(advance(parser), ==, '\n');

	munit_assert_null(str = readrocmd(parser, &len));
	munit_assert_int(te_status, ==, ERR_USAGE);
	munit_assert_int(advance(parser), ==, EOF);

	cmd_parser_free(parser);
	return MUNIT_OK;
}

static MunitResult
test_parser_readmltext(const MunitParameter params[], void *userdata)
{
	cmd_parser_t *parser = cmd_parser_string("   first line\n.b bold\n.\n.\nThis the en");
	char *str;
	size_t len;

	(void)params;
	(void)userdata;

	if(parser == NULL)
		return MUNIT_ERROR;

	munit_assert_not_null(str = readmltext(parser, &len));
	munit_assert_size(len, ==, 22);
	munit_assert_string_equal(str, "   first line\n.b bold\n");
	munit_assert_int(advance(parser), ==, '\n');

	munit_assert_not_null(str = readmltext(parser, &len));
	munit_assert_size(len, ==, 0);
	munit_assert_string_equal(str, "");
	munit_assert_int(advance(parser), ==, '\n');

	munit_assert_null(readmltext(parser, NULL));
	munit_assert_int(te_status, ==, ERR_USAGE);

	cmd_parser_free(parser);
	return MUNIT_OK;
}

static MunitResult
test_parser_rescue(const MunitParameter params[], void *userdata)
{
	cmd_parser_t *parser = cmd_parser_string(
		"Hi there\nThere will be an \\\n error on this logical line; print");

	(void)params;
	(void)userdata;

	if(parser == NULL)
		return MUNIT_ERROR;

	rescue(parser);
	munit_assert_int(advance(parser), ==, 'T');
	rescue(parser);
	munit_assert_int(advance(parser), ==, ' ');
	rescue(parser);
	munit_assert_int(advance(parser), ==, EOF);

	cmd_parser_free(parser);
	return MUNIT_OK;
}

static MunitTest cmd_tests[] = {
	{"/find_cmddef", test_find_cmddef, NULL, NULL, MUNIT_TEST_OPTION_NONE,
         NULL},
	{"/parser/advance-unget", test_parser_advance, NULL, NULL,
         MUNIT_TEST_OPTION_NONE, NULL},
	{"/parser/match", test_parser_match, NULL, NULL, MUNIT_TEST_OPTION_NONE,
         NULL},
	{"/parser/skipspaces", test_parser_skipspaces, NULL, NULL,
         MUNIT_TEST_OPTION_NONE, NULL},
	{"/parser/readuntil", test_parser_readuntil, NULL, NULL,
         MUNIT_TEST_OPTION_NONE, NULL},
	{"/parser/readnum", test_parser_readnum, NULL, NULL,
         MUNIT_TEST_OPTION_NONE, NULL},
	{"/parser/readcmdname", test_parser_readcmdname, NULL, NULL,
         MUNIT_TEST_OPTION_NONE, NULL},
	{"/parser/readrocmd", test_parser_readrocmd, NULL, NULL,
         MUNIT_TEST_OPTION_NONE, NULL},
	{"/parser/readmltext", test_parser_readmltext, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/parser/rescue", test_parser_rescue, NULL, NULL,
         MUNIT_TEST_OPTION_NONE, NULL},
	{NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
};

MunitSuite cmd_test_suite = {"/cmd", cmd_tests, NULL, 1,
                             MUNIT_SUITE_OPTION_NONE};
