#include <sys/types.h>
#include <sys/wait.h>

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "buffer.h"
#include "range.h"
#include "cmddef.h"
#include "stralloc.h"
#include "te.h"
#include "util.h"

/* XXX: pretty much the same as insert_cmd */
static int
append_cmd(struct range r, union cmdarg ca)
{
	struct rangeval rv;
	void *blob = ca.blob.blobp;
	size_t blobsz = ca.blob.blobsz;

	if(range_eval(r, &rv, REV_DOT) != OK)
		return ERR;

	if(buffer_insert(rv.buf, rv.end, blob, blobsz) != OK)
		return ERR;

	buffer_setdot(rv.buf, rv.end, rv.end + blobsz);
	return OK;
}

static int
handlepstat(int pstat)
{
	if(WIFEXITED(pstat)) {
		if(WEXITSTATUS(pstat) == 0)
			return OK;
		error(WEXITSTATUS(pstat), "command exited with code %d", WEXITSTATUS(pstat));
	} else if(WIFSIGNALED(pstat)) {
		error(256+WTERMSIG(pstat), "command terminated by signal %d", WTERMSIG(pstat));
	}

	return ERR;
}

static int
bang_cmd(struct range r, union cmdarg ca)
{
	char *cmd = ca.str;

	(void)r;

	if(cmd[0] == '\0')
		cmd = "exec sh";

	return handlepstat(shellcmd(cmd, NULL, 0, NULL));
}

static int
buffer_cmd(struct range r, union cmdarg ca)
{
	char *arg = ca.str;
	buffer_t *buf;

	(void)r;

	if(arg[0] == '\0') {
		error(ERR_INTER, "prev buffer not implemented");
		return ERR;
	}

	if((buf = buffer_find(arg)) == NULL) {
		error(ERR_NOBUF, "%s: no such buffer", arg);
		return ERR;
	}

	buffer_switch(buf);
	return OK;
}

static int
change_cmd(struct range r, union cmdarg ca)
{
	struct rangeval rv;
	void *blob = ca.blob.blobp;
	size_t blobsz = ca.blob.blobsz;

	if(range_eval(r, &rv, REV_DOT) != OK)
		return ERR;

	if(buffer_change(rv.buf, rv.start, rv.end, blob, blobsz) != OK)
		return ERR;

	buffer_setdot(rv.buf, rv.start, rv.start + blobsz);
	return OK;
}

static int
delete_cmd(struct range r, union cmdarg ca)
{
	struct rangeval rv;

	(void)ca;

	if(range_eval(r, &rv, REV_DOT) != OK)
		return ERR;

	if(buffer_delete(rv.buf, rv.start, rv.end) != OK)
		return ERR;

	buffer_setdot(rv.buf, rv.start, rv.start);
	return OK;
}

static int
edit_cmd(struct range r, union cmdarg ca)
{
	char *arg = ca.str;
	buffer_t *buf;

	(void)r;

	if((buf = buffer_find(arg)) != NULL)
		return buffer_cmd(r, ca);

	if((buf = buffer_open(arg)) == NULL)
		return ERR;

	buffer_switch(buf);
	return OK;
}

static int
file_cmd(struct range r, union cmdarg ca)
{
	char *arg = ca.str;
	char *filename;
	buffer_t *buf;

	(void)r;

	if((buf = buffer_current()) == NULL) {
		error(ERR_INTER, "no current buffer");
		return ERR;
	}

	if(arg[0] == '\0') {
		filename = buffer_filename(buf);

		printf("%s\n", filename == NULL || filename[0] == '#' ? "no filename" : filename);
	} else {
		if(buffer_setfilename(buf, arg) != OK)
			return ERR;
	}

	return OK;
}

static int
fill_cmd(struct range r, union cmdarg ca)
{
	char *cmd = ca.str;
	struct rangeval rv;
	static struct stralloc sa = { NULL, 0, 0 };

	if(range_eval(r, &rv, REV_DOT) != OK)
		return ERR;

	if(sa.s == NULL) {
		if(!stralloc_init(&sa, 28)) {
			error(ERR_OS, "out of memory");
			return ERR;
		}
	} else
		stralloc_zero(&sa);

	int ret = handlepstat(shellcmd(cmd, NULL, 0, &sa));
	if(buffer_insert(rv.buf, rv.start, sa.s, sa.n) != OK)
		return ERR;

	buffer_setdot(rv.buf, rv.start, rv.start + sa.n);
	return ret;
}

static int
insert_cmd(struct range r, union cmdarg ca)
{
	struct rangeval rv;
	void *blob = ca.blob.blobp;
	size_t blobsz = ca.blob.blobsz;

	if(range_eval(r, &rv, REV_DOT) != OK)
		return ERR;

	if(buffer_insert(rv.buf, rv.start, blob, blobsz) != OK)
		return ERR;

	buffer_setdot(rv.buf, rv.start, rv.start + blobsz);
	return OK;
}

static int
pour_cmd(struct range r, union cmdarg ca)
{
	char *cmd = ca.str;
	struct rangeval rv;

	if(range_eval(r, &rv, REV_DOT) != OK)
		return ERR;

	char data[rv.end - rv.start + 1];
		if(buffer_read(rv.buf, data, rv.start, rv.end) != OK)
			return ERR;

		int ret = handlepstat(shellcmd(cmd, data, rv.end - rv.start, NULL));
		buffer_setdot(rv.buf, rv.start, rv.end);
		return ret;
}

static int
print_cmd(struct range r, union cmdarg ca)
{
	struct rangeval rv;

	(void)ca;

	if(range_eval(r, &rv, REV_DOT) != OK)
		return ERR;

	if(buffer_write(rv.buf, 1, rv.start, rv.end) != OK)
		return ERR;

	buffer_setdot(rv.buf, rv.start, rv.end);
	return OK;
}

static int
quit_cmd(struct range r, union cmdarg ca)
{
	char *arg = ca.str;
	char *endp;
	int forced, val;

	(void)r;

	if(arg[0] == '!') {
		forced = 1;
		te_status = 0;
		arg++;
	} else {
		forced = 0;
	}

	if(!forced && !te_canquit())
		return ERR;

	if(arg[0] != '\0') {
		val = (int)strtol(arg, &endp, 10);
		if(*endp != '\0') {
			error(ERR_USAGE, "%s: invalid argument", arg);
			return ERR;
		}
		te_status = val;
	}

	te_exit();
	return OK;
}

static int
write_cmd(struct range r, union cmdarg ca)
{
	struct rangeval rv;
	char *arg = ca.str;
	int fd;

	/*
	 * Ok let me try to explain what this monstrosity tries to do:
	 *
	 * when it's invoked as just "w" it should buffer_save()
	 * otherwise it should just buffer_write().
	 * In particular ",w", "w osmefile"
	 * where "somefile" is the filename of the buffer
	 * fall under the second case.
	 *
	 * The difficulty is in checking conditions for buffer_save() case
	 * both addresses have to be unspecified, we're ok if the buffer is
	 * specified if it's the only thing specified,
	 * and arg must be empty.
	 *
	 * We could use the buffer evaluation rules from range_eval
	 * but MUH modularity and an extra call doesn't cost that much anyway
	 *
	 * this code is complicated because it is,
	 * and I feel ashamed. Ken would be sad.
	 */

	if(range_eval(r, &rv, REV_WHOLE) != OK)
		return ERR;

	if(arg[0] == '\0' && r.start.at == ADDRT_UNSPEC && r.end.at == ADDRT_ONLY1) {
		/* the buffer_save() case */
		return buffer_save(rv.buf);
	}

	if(arg[0] == '\0')
		arg = buffer_filename(rv.buf);

	if((fd = open(arg, O_WRONLY | O_CREAT | O_TRUNC, 0666)) == -1) {
		error(ERR_OS, "open %s: %s", arg, strerror(errno));
		return ERR;
	}

	if(buffer_write(rv.buf, fd, rv.start, rv.end) != OK)
		return ERR;

	buffer_setdot(rv.buf, rv.start, rv.end);
	close(fd);
	return OK;
}

static struct cmddef cmddefs[] = {
	{"!", bang_cmd, CMDAT_STR},
	{"<", fill_cmd, CMDAT_STR},
	{">", pour_cmd, CMDAT_STR},
	{"a", append_cmd, CMDAT_TEXT},
	{"append", append_cmd, CMDAT_TEXT},
	{"bang", bang_cmd, CMDAT_STR},
	{"b", buffer_cmd, CMDAT_STR},
	{"buffer", buffer_cmd, CMDAT_STR},
	{"c", change_cmd, CMDAT_TEXT},
	{"change", change_cmd, CMDAT_TEXT},
	{"d", delete_cmd, CMDAT_NONE},
	{"delete", delete_cmd, CMDAT_NONE},
	{"e", edit_cmd, CMDAT_STR},
	{"edit", edit_cmd, CMDAT_STR},
	{"f", file_cmd, CMDAT_STR},
	{"file", file_cmd, CMDAT_STR},
	{"fill", fill_cmd, CMDAT_STR},
	{"i", insert_cmd, CMDAT_TEXT},
	{"insert", insert_cmd, CMDAT_TEXT},
	{"p", print_cmd, CMDAT_NONE},
	{"pour", pour_cmd, CMDAT_STR},
	{"print", print_cmd, CMDAT_NONE},
	{"q", quit_cmd, CMDAT_STR},
	{"quit", quit_cmd, CMDAT_STR},
	{"w", write_cmd, CMDAT_STR},
	{"write", write_cmd, CMDAT_STR},
};

static int
find_cmddef_compare(const void *a, const void *b)
{
	const char *cmdname = a;
	const struct cmddef *cmddef = b;

	return strcmp(cmdname, cmddef->name);
}

struct cmddef *
find_cmddef(const char *name)
{
	return bsearch(name, cmddefs, sizeof(cmddefs) / sizeof(cmddefs[0]),
	               sizeof(cmddefs[0]), find_cmddef_compare);
}
