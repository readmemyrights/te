#ifndef CMDDEF_H
#define CMDDEF_H

/* range.h needs to be included */

#define CMDNAME_MAX 32

enum cmdargtype {
	CMDAT_NONE,
	CMDAT_STR,
	CMDAT_TEXT,
};

union cmdarg {
	char *str;
	struct {
		void *blobp;
		size_t blobsz;
	} blob;
};

struct cmddef {
	const char name[CMDNAME_MAX];
	int (*fn)(struct range, union cmdarg);
	enum cmdargtype type;
};

struct cmddef *find_cmddef(const char *);

#endif
