#include <stdio.h>

#include "buffer.h"
#include "range.h"
#include "te.h"

void
address_dump(struct address addr)
{
	switch(addr.at) {
	case ADDRT_UNSPEC:
		break;
	case ADDRT_DOT:
		fputc('.', stderr);
		break;
	case ADDRT_OFFSET:
		fprintf(stderr, "@%zu", addr.v.offset);
		break;
	case ADDRT_LINE:
		fprintf(stderr, "%zu", addr.v.line);
		break;
	case ADDRT_LAST:
		fputc('$', stderr);
		break;
	default:
		fputc('?', stderr);
		break;
	}
}

void
range_dump(struct range r)
{
	if(r.bufname != NULL)
		fprintf(stderr, "\"%s\" ", r.bufname);

	address_dump(r.start);
	if(r.end.at != ADDRT_ONLY1) {
		fputc(',', stderr);
		address_dump(r.end);
	}
}

int
range_eval(struct range r, struct rangeval *rvp, int flags)
{
	buffer_t *buf;
	size_t start, end;

	buf = r.bufname == NULL ? buffer_current() : buffer_find(r.bufname);
	if(buf == NULL) {
		error(ERR_NOBUF, "%s: no such buffer", r.bufname);
		return ERR;
	}

	switch(r.start.at) {
	case ADDRT_UNSPEC:
		/* if neither address was provided in any way, something
		 * like "p" and not ",p" */
		if(r.end.at == ADDRT_ONLY1 && (flags & REV_DOT))
			goto dotaddr;
		else
			goto wholeaddr;
		break;
	case ADDRT_ONLY1:
		error(ERR_INTER,
		      "ADDRT_ONLY1 can't happen for first address");
		return ERR;
	wholeaddr:
		start = 0;
		end = buffer_length(buf);
		break;
	case ADDRT_DOT:
	dotaddr:
		buffer_getdot(buf, &start, &end);
		break;
	case ADDRT_OFFSET:
		start = end = r.start.v.offset;
		break;
	case ADDRT_LINE:
		if(buffer_getline(buf, r.start.v.line, &start, &end) != OK)
			return ERR;
		break;
	case ADDRT_LAST:
		start = end = buffer_length(buf);
		break;
	default:
		error(ERR_INTER, "call brisco: unknown address type %d",
		      r.start.at);
		return ERR;
	}

	switch(r.end.at) {
	case ADDRT_UNSPEC:
		end = buffer_length(buf);
		break;
	case ADDRT_ONLY1:
		break;
	case ADDRT_DOT:
		buffer_getdot(buf, NULL, &end);
		break;
	case ADDRT_OFFSET:
		end = r.end.v.offset;
		break;
	case ADDRT_LINE:
		if(buffer_getline(buf, r.end.v.line, NULL, &end) != OK)
			return ERR;
		break;
	case ADDRT_LAST:
		end = buffer_length(buf);
		break;
	default:
		error(ERR_INTER,
		      "call Brisco: unexpected address type %d",
		      r.end.at);
		return ERR;
	}

	if(end < start) {
		error(ERR_OOR, "end < start");
		return ERR;
	}

	rvp->buf = buf;
	rvp->start = start;
	rvp->end = end;
	return OK;
}
