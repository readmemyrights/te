#ifndef RANGE_H
#define RANGE_H

/* buffer.h needs to be included */

enum addrtype {
	ADDRT_UNSPEC,
	ADDRT_ONLY1,
	ADDRT_DOT,
	ADDRT_OFFSET,
	ADDRT_LINE,
	ADDRT_LAST,
};

struct address {
	enum addrtype at;
	union {
		size_t offset;
		size_t line;
	} v;
};

struct range {
	char *bufname;
	struct address start, end;
};

struct rangeval {
	buffer_t *buf;
	size_t start, end;
};

#define REV_WHOLE 0
#define REV_DOT 1

void address_dump(struct address addr);
void range_dump(struct range r);
int range_eval(struct range r, struct rangeval *rvp, int flags);

#endif
