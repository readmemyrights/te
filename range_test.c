#include "munit/munit.h"

#include <stdio.h>

#include "buffer.h"
#include "range.h"
#include "te.h"

static MunitResult
test_range_eval(const MunitParameter params[], void *userdata)
{
	buffer_t *curbuf = buffer_string("www");
	buffer_t *otherbuf = buffer_string("xxxxxxxx");
	int failed;
	const struct {
		struct range input;
		int flags;
		struct rangeval output;
		int ret;
		int line;
	} tests[] = {
		{
			.line = __LINE__,
			.input = {NULL,
	                          {.at = ADDRT_UNSPEC},
	                          {.at = ADDRT_UNSPEC}},
			.flags = REV_WHOLE,
			.output = {curbuf, 0, 3},
			.ret = OK,
		},
		{
			.line = __LINE__,
			.input =
				{
					.bufname = "other buffer",
					.start = {ADDRT_OFFSET, {.offset = 42}},
					.end = {ADDRT_OFFSET, {.offset = 49}},
				},
			.output = {otherbuf, 42, 49},
			.ret = OK,
		},
		{
			.line = __LINE__,
			.input =
				{
					.bufname = "current",
					.start = {ADDRT_OFFSET, {.offset = 43}},
					.end = {.at = ADDRT_ONLY1},
				},
			.output = {curbuf, 43, 43},
			.ret = OK,
		},
		{
			.input =
				{
					.bufname = "other buffer",
					.start = {ADDRT_OFFSET, {.offset = 13}},
					.end = {.at = ADDRT_UNSPEC},
				},
			.ret = ERR,
		},
		{
			.line = __LINE__,
			.input = {
				.bufname = "current",
				.start = {.at = ADDRT_LAST},
				.end = {.at = ADDRT_UNSPEC}
			},
			.output = {curbuf, 3, 3},
			.ret = OK,
		},
		{
			.line = __LINE__,
			.input = {
				.bufname = "other buffer",
				.start = {.at = ADDRT_OFFSET, {.offset = 3}},
				.end = {.at = ADDRT_LAST},
			},
			.output = {otherbuf, 3, 8},
			.ret = OK,
		},
	};

	(void)params;
	(void)userdata;

	if(curbuf == NULL
			|| otherbuf == NULL
			|| buffer_setfilename(curbuf, "current") != OK
			|| buffer_setfilename(otherbuf, "other buffer") != OK)
		return MUNIT_ERROR;

	failed = 0;
	for(int i = 0; (size_t)i < sizeof(tests) / sizeof(tests[0]); i++) {
		struct rangeval output;
		int ret = range_eval(tests[i].input, &output, tests[i].flags);
		if(ret != tests[i].ret) {
			fprintf(stderr,
			        "%s:%d: test %d failed, expected %s got %s\n",
			        __FILE__, tests[i].line, i,
			        tests[i].ret == OK ? "success" : "failure",
			        ret == OK ? "success" : "failure");
			failed = 1;
		}
		if(ret != OK)
			continue;
		if(output.buf != tests[i].output.buf) {
			fprintf(stderr,
			        "%s:%d: test %d failed, expected %p got %p\n",
			        __FILE__, tests[i].line, i, tests[i].output.buf,
			        output.buf);
			failed = 1;
		}
		if(output.start != tests[i].output.start) {
			fprintf(stderr,
			        "%s:%d: test %d failed, expected %zu got %zu\n",
			        __FILE__, tests[i].line, i,
			        tests[i].output.start, output.start);
			failed = 1;
		}
		if(output.end != tests[i].output.end) {
			fprintf(stderr,
			        "%s:%d: test %d failed, expected %zu "
			        "got %zu\n",
			        __FILE__, tests[i].line, i, tests[i].output.end,
			        output.end);
			failed = 1;
		}
	}

	return failed ? MUNIT_FAIL : MUNIT_OK;
}

static MunitTest range_tests[] = {
	{"/eval", test_range_eval, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
};

MunitSuite range_test_suite = {"/range", range_tests, NULL, 1,
                               MUNIT_SUITE_OPTION_NONE};
