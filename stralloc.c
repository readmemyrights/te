#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "stralloc.h"

int
stralloc_init(struct stralloc *sa, size_t n)
{
	memset(sa, 0, sizeof *sa);
	if(n > 0) {
		sa->s = malloc(n);
		if(sa->s == NULL)
			return 0;
		sa->a = n;
	}
	return 1;
}

void
stralloc_zero(struct stralloc *sa)
{
	memset(sa->s, sa->a, sizeof(*sa->s));
	sa->n = 0;
}

int
stralloc_ready(struct stralloc *sa, size_t n)
{
	if(n <= sa->a)
		return 1;

	size_t wanted =
		n + (n >> 3) + 30; /* DJB heuristic; I have no idea why */
	char *news = realloc(sa->s, wanted);
	if(news == NULL)
		return 0;
	sa->s = news;
	sa->a = wanted;
	return 1;
}

int
stralloc_append(struct stralloc *sa, int c)
{
	if(!stralloc_readyplus(sa, 1))
		return 0;

	sa->s[sa->n++] = c;
	return 1;
}

int
stralloc_copyb(struct stralloc *sa, void *buf, size_t bufsz)
{
	if(!stralloc_ready(sa, bufsz + 1))
		return 0;

	memmove(sa->s, buf, bufsz);
	sa->n = bufsz;
	return 1;
}

int
stralloc_catb(struct stralloc *sa, void *buf, size_t bufsz)
{
	if(!stralloc_readyplus(sa, bufsz + 1))
		return 0;

	memmove(sa->s + sa->n, buf, bufsz);
	sa->n += bufsz;
	return 1;
}

int
stralloc_insertb(struct stralloc *sa, size_t pos, void *buf, size_t bufsz)
{
	if(pos > sa->n) {
		errno = EINVAL;
		return 0;
	}

	if(pos == sa->n)
		return stralloc_catb(sa, buf, bufsz);

	if(!stralloc_readyplus(sa, bufsz))
		return 0;

	memmove(sa->s + pos + bufsz, sa->s + pos, sa->n - pos);
	memmove(sa->s + pos, buf, bufsz);
	sa->n += bufsz;

	return 1;
}

int
stralloc_catl(struct stralloc *sa, ...)
{
	va_list ap;

	va_start(ap, sa);
	int r = stralloc_vcatl(sa, ap);
	va_end(ap);

	return r;
}

int
stralloc_vcatl(struct stralloc *sa, va_list ap)
{
	char *arg;

	while((arg = va_arg(ap, char *)) != NULL)
		if(!stralloc_cats(sa, arg))
			return 0;

	return 1;
}

int
stralloc_printf(struct stralloc *sa, const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	int r = stralloc_vprintf(sa, fmt, ap);
	va_end(ap);

	return r;
}

int
stralloc_vprintf(struct stralloc *sa, const char *fmt, va_list ap)
{
	char buf[1024]; /* Let me guess, you "need" more? */

	int r = vsnprintf(buf, sizeof buf, fmt, ap);
	if(r < 0)
		return 0;
	if((size_t)r > sizeof buf) {
		errno = ERANGE;
		return 0;
	}

	return stralloc_copyb(sa, buf, sizeof buf);
}

int
stralloc_catf(struct stralloc *sa, const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	int r = stralloc_vcatf(sa, fmt, ap);
	va_end(ap);

	return r;
}

int
stralloc_vcatf(struct stralloc *sa, const char *fmt, va_list ap)
{
	char buf[1024];

	int r = vsnprintf(buf, sizeof buf, fmt, ap);
	if(r < 0)
		return 0;
	if((size_t)r > sizeof buf) {
		errno = ERANGE;
		return 0;
	}

	return stralloc_catb(sa, buf, sizeof buf);
}
