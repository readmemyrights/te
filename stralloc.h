#ifndef STRALLOC_H
#define STRALLOC_H

#include <stdarg.h>
#include <stddef.h>

struct stralloc {
	char *s;
	size_t n;
	size_t a;
};

int stralloc_init(struct stralloc *sa, size_t n);
void stralloc_zero(struct stralloc *sa);
int stralloc_ready(struct stralloc *sa, size_t n);
#define stralloc_readyplus(sa, n) stralloc_ready(sa, (sa)->a + (n))
int stralloc_append(struct stralloc *sa, int c);
int stralloc_copyb(struct stralloc *sa, void *buf, size_t bufsz);
#define stralloc_copys(sa, s) stralloc_copyb(sa, s, strlen(s))
/* XXX: double evaluation of sasrc */
#define stralloc_copysa(sa, sasrc) stralloc_copyb(sa, (sasrc)->s, (sasrc)->n)
int stralloc_catb(struct stralloc *sa, void *buf, size_t bufsz);
#define stralloc_cats(sa, s) stralloc_catb(sa, s, strlen(s))
#define stralloc_catsa(sa, sasrc) stralloc_catb(sa, (sasrc)->s, (sasrc)->n)
int stralloc_insertb(struct stralloc *sa, size_t pos, void *buf, size_t bufsz);
#define stralloc_inserts(sa, pos, s) stralloc_insertb(sa, pos, str, strlen(s))
#define stralloc_insertsa(sa, pos, sasrc) stralloc_insertb(sa, pos, (sasrc)->s, (sasrc)->n)
int stralloc_catl(struct stralloc *sa, /* char *s */... /*, (char *)0*/);
int stralloc_vcatl(struct stralloc *sa, va_list ap);
int stralloc_printf(struct stralloc *sa, const char *fmt, ...);
int stralloc_vprintf(struct stralloc *sa, const char *fmt, va_list ap);
int stralloc_catf(struct stralloc *sa, const char *fmt, ...);
int stralloc_vcatf(struct stralloc *sa, const char *fmt, va_list ap);
#endif
