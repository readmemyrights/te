#include "munit/munit.h"
#include "stralloc.h"

static MunitResult
test_stralloc_init(const MunitParameter params[], void *userdata)
{
	struct stralloc sa;
	size_t n;

	(void)params;
	(void)userdata;

	n = munit_rand_int_range(0, 1024);
	if(!stralloc_init(&sa, n))
		return MUNIT_ERROR;

	free(sa.s);
	munit_assert_not_null(sa.s);
	munit_assert_size(sa.a, ==, n);
	munit_assert_size(sa.n, ==, 0);
	return MUNIT_OK;
}

static MunitResult
test_stralloc_ready(const MunitParameter params[], void *userdata)
{
	struct stralloc sa;
	size_t n, more;

	(void)params;
	(void)userdata;

	n = munit_rand_int_range(8, 1024);
	if(!stralloc_init(&sa, n))
		return MUNIT_ERROR;

	munit_assert_true(stralloc_ready(&sa, 8)); /* should always succeed */

	more = n + munit_rand_int_range(1, 32);
	if(!stralloc_ready(&sa, more)) {
		free(sa.s);
		return MUNIT_ERROR;
	}

	free(sa.s);
	munit_assert_size(more, <=, sa.a);
	munit_assert_size(sa.n, ==, 0);
	return MUNIT_OK;
}

static MunitResult
test_stralloc_copy(const MunitParameter params[], void *userdata)
{
	struct stralloc sa;
	unsigned char buf[64];
	size_t wherenull;

	(void)params;
	(void)userdata;

	if(!stralloc_init(&sa, 16))
		return MUNIT_ERROR;

	munit_rand_memory(sizeof buf, buf);
	if(!stralloc_copyb(&sa, buf, sizeof buf))
		goto error;

	munit_assert_size(sizeof buf, ==, sa.n);
	munit_assert_memory_equal(sa.n, sa.s, buf);

	wherenull = munit_rand_int_range(0, 63);
	/*
	 * munit_rand_memory can generate null bytes
	 * which causes trouble with stralloc_{copy,cat}s.
	 *
	 * The for loop is a hack
	 * that fixes this specific problem.
	 *
	 * To repeat: this isn't a bug in stralloc_copys,
	 * this is a bug in null terminated strings existing
	 */
	for(size_t i = 0; i < wherenull; i++)
		if(buf[i] == '\0')
			buf[i] = 'z';
	buf[wherenull] = '\0';
	/* have to explicitly null terminate */
	if(!stralloc_copys(&sa, (char *)buf) || !stralloc_append(&sa, '\0'))
		goto error;

	munit_assert_size(sa.n, ==, wherenull + 1);
	munit_assert_string_equal((char *)buf, sa.s);

	free(sa.s);
	return MUNIT_OK;

error:
	free(sa.s);
	return MUNIT_ERROR;
}

static MunitResult
test_stralloc_cat(const MunitParameter params[], void *userdata)
{
	struct stralloc sa;
	unsigned char buf[64];
	char rbuf[128];
	size_t wherenull;

	(void)params;
	(void)userdata;

	if(!stralloc_init(&sa, 16))
		return MUNIT_ERROR;

	munit_rand_memory(sizeof buf, buf);
	/* see comment above a similar for loop in test_stralloc_copy */
	for(size_t i = 0; i < sizeof buf; i++)
		if(buf[i] == '\0')
			buf[i] = 'z';
	if(!stralloc_catb(&sa, buf, sizeof buf))
		goto error;
	munit_assert_size(sa.n, ==, sizeof buf);
	munit_assert_memory_equal(sizeof buf, buf, sa.s);

	/* These strcpys and strcats won't overflow the buffer; I know this */
	memcpy(rbuf, buf, sizeof buf);
	rbuf[sizeof buf] = '\0';
	wherenull = munit_rand_int_range(0, 63);
	buf[wherenull] = '\0';
	if(!stralloc_cats(&sa, (char *)buf))
		goto error;
	strcat(rbuf, (char *)buf);
	munit_assert_size(sa.n, ==, sizeof(buf) + wherenull);
	munit_assert_memory_equal(sa.n, sa.s, rbuf);

	free(sa.s);
	return MUNIT_OK;

error:
	free(sa.s);
	return MUNIT_ERROR;
}

static MunitResult
test_stralloc_insert(const MunitParameter params[], void *userdata)
{
	struct stralloc sa;
	unsigned char buf[64];
	size_t where;

	(void)params;
	(void)userdata;

	if(!stralloc_init(&sa, 16))
		return MUNIT_ERROR;

	munit_rand_memory(sizeof buf, buf);
	if(!stralloc_copyb(&sa, buf, sizeof buf))
		goto error;

	munit_assert_false(stralloc_insertb(&sa, 98, buf, sizeof buf));
	munit_assert_true(stralloc_insertb(&sa, sa.n, buf, sizeof buf));
	munit_assert_memory_equal(sizeof buf, buf, sa.s);
	munit_assert_memory_equal(sizeof buf, buf, sa.s + sizeof buf);

	where = munit_rand_int_range(1, 63);
	munit_assert_true(stralloc_insertb(&sa, where, buf, sizeof buf));
	munit_assert_memory_equal(where, buf, sa.s);
	munit_assert_memory_equal(sizeof buf, buf, sa.s + where);
	munit_assert_memory_equal(sizeof(buf) - where, buf + where, sa.s + where + sizeof(buf));

	free(sa.s);
	return MUNIT_OK;

error:
	free(sa.s);
	return MUNIT_ERROR;
}

static MunitTest stralloc_tests[] = {
	{"/init", test_stralloc_init, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/ready", test_stralloc_ready, NULL, NULL, MUNIT_TEST_OPTION_NONE,
         NULL},
	{"/copy", test_stralloc_copy, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/cat", test_stralloc_cat, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{"/insert", test_stralloc_insert, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
	{NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL},
};

MunitSuite stralloc_test_suite = {"/stralloc", stralloc_tests, NULL, 1,
                                  MUNIT_SUITE_OPTION_NONE};
