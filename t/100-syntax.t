#!/bin/sh

nl='
'
tab='	'

cd "${0%/*}"
. ./lib.sh
plan 7
te_check 'do nothing if told nothing' '' '' 0
te_check 'except various forms of valid syntax' "  p; @0, @10print    $tab$nl.,.quit" "p${nl}@0,@10print${nl}.,.quit" 0 -D -n
te_check 'leave out a comma' '@0.p' 'te: unexpected . after command' 100
te_check 'run an unknown command' 'rpint' 'te: rpint: no such command' 127
te_check 'catch nonsensical range' '@5, @2p' 'te: end < start' 101
printf 'q bl\0b\n' |te_check 'no NUL in string argument' - 'te: NUL byte in string argument' 100
# Actually, it shouldn't error out on non-existent files, it should make one
# instead
te_check 'do not error out on nonexistent file' 'p' '' 0 /non/exist
