#!/bin/sh

cd "${0%/*}"
. ./lib.sh

cat >200-print.1.tmp <<EOF
He was the strongest of them all
Born to fight the ancient war
Between aliens and dinosaurs.
EOF

cat >200-print.2.tmp <<EOF
It is necessary for the welfare of society that genius should be privileged
to utter sedition, to blaspheme, to outrage good taste, to corrupt the
youthful mind, and generally to scandalize one's uncles.
		-- George Bernard Shaw
EOF

plan 6
te_check 'by default the dot is empty' 'p' '' 0 200-print.1.tmp
te_check 'printing offset ranges' '@0,@6print' 'He was' 0 200-print.1.tmp
te_check 'dot is the last range printed, and it'\''s the default' '@0,@6p; print' 'He wasHe was' 0 200-print.1.tmp
te_check 'start must come before end' '@13,@8p' 'te: end < start' 101
te_check 'multiple buffers' '@7,@20p
"200-print.2.tmp" @209,$p
"200-print.1.tmp"
"200-print.2.tmp" print' 'the strongestGeorge Bernard Shaw
the strongestGeorge Bernard Shaw' 0 200-print.1.tmp 200-print.2.tmp
te_check 'printing lines' '1p
2p
3p
0p
4p' 'He was the strongest of them all
Born to fight the ancient war
Between aliens and dinosaurs.
He was the strongest of them all
te: line out of range' 101 200-print.1.tmp
