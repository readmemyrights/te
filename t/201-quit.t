#!/bin/sh

nl='
'

cd "${0%/*}"
. ./lib.sh

plan 9
te_check 'quits and stops reading' 'q; rpint' '' 0
te_check 'exits with error code' 'rpint;quit' 'te: rpint: no such command' 127
te_check 'does not exit with error code if successful command' 'rpint; print' 'te: rpint: no such command' 0
te_check 'exits with given error code' 'q123' '' 123
te_check 'exits with given error code and spaces' 'quit  23' '' 23
te_check 'does not quit on invalid argument' 'q nan; @2,@9p' 'te: nan: invalid argument'"$nl"'te: out of range' 101
te_check 'does not quit if there are dirty buffers' 'i test; q' 'te: you have unsaved buffers' 100
te_check 'does not quit if there are dirty buffers, multiple buffers' '@0i hello; q' 'te: you have unsaved buffers' 100 100-syntax.t 201-quit.t
te_check 'quit!s if there are dirty buffers' 'i test; q!' '' 0
