#!/bin/sh

cd "${0%/*}"
. ./lib.sh

plan 7
te_check 'basic insertion' 'i hello; @0,@5p' 'hello' 0
te_check 'basic appending' 'a hello; @0,@5p' 'hello' 0
te_check 'dot is set to the inserted text' 'i fnord; p' 'fnord' 0
te_check 'dot is set to the appended text' 'a fnord; p' 'fnord' 0
te_check 'multi line text' 'i
He was the strongest of them all
Born to fight the ancient war
Between aliens and dinosaurs
.
@29,@37print' 'all
Born' 0
te_check 'end of the range doesn'\''t matter' 'i fruit; @0,@1i grape
@0,@14i Not a ; @0,@16p' 'Not a grapefruit' 0
te_check 'force spaces at the beginning with backslash' 'i hello; $i \ there
,p' 'hello there' 0
