#!/bin/sh

cd "${0%/*}"
. ./lib.sh

plan 3
te_check 'basic deletion' 'i The letttttttter t got stuck
@6,@12d
,p' 'The letter t got stuck' 0
te_check 'delete sets the dot' 'i The true fruit of discord
@9,@14delete
insert cabbage
,p' 'The true cabbage of discord' 0
te_check 'properly reindex lines' '<date
1d
,p' '' 0
