#!/bin/sh

cd "${0%/*}"
. ./lib.sh

echo a quick brown fox jumped over the lazy dog >204-write.1.tmp
printf %s 'a quick brown fox' >204-write.2.tmp

plan 2
te_check 'writing a portion of the buffer' '@0,@17w 204-write.3.tmp' '' 0 204-write.1.tmp
if cmp -s 204-write.2.tmp 204-write.3.tmp; then
	echo ok - the files match
else
	echo not ok - the files match
fi
