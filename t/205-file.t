#!/bin/sh

cd "${0%/*}"
. ./lib.sh

plan 2
te_check 'the file commands prints and sets' 'f
f 205-file.tmp
f
i
this is a test
.
w
q' 'no filename
205-file.tmp' 0
check 'file readable and has the right contents' 'test "$(cat 205-file.tmp)" == "this is a test"'
