#!/bin/sh

cd "${0%/*}"
. ./lib.sh

cat > 206-buffer.1.tmp <<EOF
#!/bin/sh
EOF

cat >206-buffer.2.tmp <<EOF
#!/usr/bin/env tclsh
EOF

plan 4
te_check 'simple switching' '@0,@10p
"206-buffer.2.tmp" @15,@21p
b 206-buffer.2.tmp
p
b 206-buffer.1.tmp
p' '#!/bin/sh
tclsh
tclsh
#!/bin/sh' 0 206-buffer.1.tmp 206-buffer.2.tmp
te_check 'non-existent buffers' 'b 206-buffer.3.tmp' 'te: 206-buffer.3.tmp: no such buffer' 110
te_check 'switch to previous buffer # TODO not implemented' '@0@10p
"206-buffer.2.tmp" @15,@21p
b 206-buffer.2.tmp
p
b
p' '#!/bin/sh
tclsh
tclsh
#!/bin/sh' 0 206-buffer.1.tmp 206-buffer.2.tmp
te_check 'edit command' '
e 206-buffer.1.tmp
@0,@10p
c
#!/bin/bash
.
e 206-buffer.2.tmp
@15,@21p
e 206-buffer.1.tmp
p
' '#!/bin/sh
tclsh
#!/bin/bash' 0
