#!/bin/sh

cd "${0%/*}"
. ./lib.sh

plan 3
te_check 'The ! command' '!echo hi there
,!exit 100
!kill -11 $$
' 'hi there
te: command exited with code 100
te: command terminated by signal 11' 11 # Gets clamped to 8 bits, sucks.
te_check 'the > command' 'i
fox 1
dog 2
cat 3
.
>head -1
1>tr [:lower:] [:upper:]
>sed "s/fox/vulpine/"' 'fox 1
FOX 1
vulpine 1' 0
te_check 'the < command' '<echo hi there
@0,@3p' 'hi ' 0
