# Usage: plan <number-of-tests>
plan() {
	echo "1..$1"
}

# Usage: te_check <description> <input> <output> <status> [arguments ...]
te_check() {
	desc=$1; shift
	input=$1; shift
	expected_output=$1; shift
	expected_status=$1; shift

	case "$input" in
		-) output=$(te "$@" 2>&1);;
		*) output=$(printf '%s' "$input" |te "$@" 2>&1);;
	esac
	status=$?

	if [ "$status" -ne "$expected_status" ] || [ "$output" != "$expected_output" ]; then
		printf '$?=%d\n' "$status"
printf '%s\n' "$output" |sed 's/^/	/'
		printf 'not ok - %s\n' "$desc"
	else
		printf 'ok - %s\n' "$desc"
	fi
	true
}

# Usage: check description command...
check() {
	msg=$1
	shift
	if eval "$@" 1>&2; then
		printf 'ok - %s\n' "$msg"
	else
		printf 'not ok - %s\n' "$msg"
	fi
	true
}
