#include <errno.h>
#include <getopt.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "buffer.h"
#include "cle.h"
#include "cmd.h"
#include "te.h"

static const char *progname;
char errstr[ERRSTR_MAX];
int te_status;

int te_interactive;
char *te_prompt = "* ";
char *te_contprompt = "  ";

static void
usage(void)
{
	fprintf(stderr, "Usage: %s [-DIin] [-P contprompt] [-p prompt] args\n", progname);
	exit(ERR_USAGE);
}

void
error(int status, const char *fmt, ...)
{
	va_list ap;

	te_status = status;
	va_start(ap, fmt);
	vsnprintf(errstr, sizeof errstr, fmt, ap);
	va_end(ap);
}

void
warning(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	fprintf(stderr, "%s: ", progname);
	vfprintf(stderr, fmt, ap);
	fputc('\n', stderr);
	va_end(ap);
}

int
te_exit(void)
{
	cle_deinit();
	exit(abs(te_status));
	/* NOTREACHED */
	return OK;
}

int
te_main(int argc, char *argv[])
{
	cmd_parser_t *parser;
	int c;
	int e;
	int flags = 0;
	int debug = 0, dryrun = 0, use_cle = 0;

	progname = argv[0];
	te_interactive = isatty(0) && isatty(1) && isatty(2);

	while((c = getopt(argc, argv, "DIinP:p:")) != -1)
		switch(c) {
		case 'D':
			debug = 1;
			break;
		case 'I':
			te_interactive = 0;
			break;
		case 'i':
			te_interactive = 1;
			break;
		case 'n':
			dryrun = 1;
			break;
		case 'P':
			te_contprompt = optarg;
			break;
		case 'p':
			te_prompt = optarg;
			break;
		case '?':
		default:
			usage();
		}
	argc -= optind;
	argv += optind;

	if(te_interactive) {
		if(!(use_cle = cle_init() == OK))
			warning("failed to initialize command line editing");
	} else
		use_cle = 0;

	for(int i = 0; i < argc; i++) {
		if(buffer_open(argv[i]) == NULL)
			warning("%s", errstr);
	}

	if(buffer_current() == NULL)
		if(buffer_new() == NULL) {
			warning("%s", errstr);
			te_exit();
		}

	if(use_cle)
		parser = cmd_parser_cle();
	else
		parser = cmd_parser_fd(0);
	if(parser == NULL) {
		fprintf(stderr, "%s: cannot initialize parser: %s\n", progname,
		        strerror(errno));
		exit(ERR_OS);
	}
	if(debug)
		flags |= CLO_DEBUG;
	if(dryrun)
		flags |= CLO_DRYRUN;
	te_status = 0;
	for(;;) {
		if((e = cmd_loop(parser, flags)) == NOMOCMD)
			break;

		if(e == ERR)
			fprintf(stderr, "%s: %s\n", progname, errstr);
	}

	cmd_parser_free(parser);
	te_exit();
	return 0;
}

int
te_canquit(void)
{
	buffer_t *buf;

	BUFFER_FOREACH(buf)
		if(buffer_dirty(buf)) {
			error(ERR_USAGE, "you have unsaved buffers");
			return 0;
		}

	return 1;
}
