#ifndef TE_H
#define TE_H

#define OK 0
#define ERR 1
#define NOMOCMD 2
#define ERR_INTER 99
#define ERR_USAGE 100
#define ERR_OOR 101
#define ERR_NOBUF 110
#define ERR_OS 111
#define ERR_NOCMD 127

#define ERRSTR_MAX 1024

extern int te_status;
extern char errstr[ERRSTR_MAX];

extern int te_interactive;
extern char *te_prompt;
extern char *te_contprompt;

void error(int status, const char *fmt, ...);
void warning(const char *fmt, ...);
int te_exit(void);
int te_main(int argc, char **argv);
int te_canquit(void);

#endif
