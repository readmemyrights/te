#include "munit/munit.h"

extern MunitSuite stralloc_test_suite;
extern MunitSuite buffer_test_suite;
extern MunitSuite cmd_test_suite;
extern MunitSuite range_test_suite;
extern MunitSuite util_testsuite;

int
main(int argc, char *argv[])
{
	MunitSuite all_test_suites[] = {
		stralloc_test_suite,
		buffer_test_suite,
		cmd_test_suite,
		range_test_suite,
		util_testsuite,
		{NULL, NULL, NULL, 0, MUNIT_SUITE_OPTION_NONE},
	};

	MunitSuite whole_test_suite = {"", NULL, all_test_suites, 1,
	                               MUNIT_SUITE_OPTION_NONE};

	return munit_suite_main(&whole_test_suite, NULL, argc, argv);
}
