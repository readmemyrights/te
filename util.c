#include <sys/wait.h>

#include <errno.h>
#include <stddef.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>

#include "stralloc.h"
#include "util.h"

/*
 * Runs "sh -c cmd" as a child process,
 * writes insz bytes from in into it,
 * and appends its output to outsa.
 *
 * The code is shamelessly copied from OpenBSD's system.c.
 * That will bite me in the ass I just know it.
 */
int
shellcmd(char *cmd, void *in, size_t insz, struct stralloc *outsa)
{
	pid_t cpid, pid;
	struct sigaction sa, intsave, quitsave, pipesave;
	sigset_t mask, omask;
	int havein, haveout;
	int inpipe[2], outpipe[2];
	int pstat;
	char *argv[] = { "sh", "-c", cmd, NULL };

	sigemptyset(&mask);
	sigaddset(&mask, SIGINT);
	sigaddset(&mask, SIGQUIT);
	sigaddset(&mask, SIGCHLD);
	sigprocmask(SIG_BLOCK, &mask, &omask);

	havein = in != NULL && insz > 0;
	haveout = outsa != NULL;

	if(havein) {
		if(pipe(inpipe) == -1) {
			sigprocmask(SIG_SETMASK, &omask, NULL);
			return -1;
		}
	}

	if(haveout) {
		if(pipe(outpipe) == -1) {
			if(havein)
				close(inpipe[0]), close(inpipe[1]);
			sigprocmask(SIG_SETMASK, &omask, NULL);
			return -1;
		}
	}

	switch(cpid = fork()) {
		case -1:
			if(havein)
				close(inpipe[0]), close(inpipe[1]);
			if(haveout)
				close(outpipe[0]), close(outpipe[1]);
			sigprocmask(SIG_SETMASK, &omask, NULL);
			return -1;
		case 0:
			/*
			 * We don't need to clean up here,
			 * os janny will do it for fre.
			 * */
			if(havein) {
				if(dup2(inpipe[0], 0) == -1)
					_exit(126);
				close(inpipe[0]);
				close(inpipe[1]);
			}
			if(haveout) {
				if(dup2(outpipe[1], 1) == -1)
					_exit(126);
				close(outpipe[0]);
				close(outpipe[1]);
			}
			sigprocmask(SIG_SETMASK, &omask, NULL);
			/*
			 * if /bin/sh isn't a posix shell on your system,
			 * consider restricting yourself from computers
			 */
			execv("/bin/sh", argv);
			_exit(errno == ENOENT ? 127 : 126);
	}

	memset(&sa, 0, sizeof sa);
	sigemptyset(&sa.sa_mask);
	sa.sa_handler = SIG_IGN;
	sigaction(SIGINT, &sa, &intsave);
	sigaction(SIGQUIT, &sa, &quitsave);
	sigaction(SIGPIPE, &sa, &pipesave);
	sigemptyset(&mask);
	sigaddset(&mask, SIGINT);
	sigaddset(&mask, SIGQUIT);
	sigprocmask(SIG_UNBLOCK, &mask, NULL);

	if(havein) {
		close(inpipe[0]);
		int fd = inpipe[1];
		ssize_t ret;
		size_t off = 0;

		while(off < insz) {
			ret = write(fd, in + off, insz - off);
			if(ret == -1) {
				if(errno == EINTR)
					continue;
				else
					break;
			}
			off += ret;
		}

		close(fd);
	}

	if(haveout) {
		char buf[512];
		ssize_t ret;
		int fd = outpipe[0];

		close(outpipe[1]);

		while((ret = read(fd, buf, sizeof buf)) > 0) {
			/*
			 * We can't really do anything about errors here,
			 * or erros when reading.
			 * By now restoring the original state of the stralloc
			 * is pretty much impossible,
			 * without getting into what the command could have
			 * potentially done.
			 *
			 * If we run out of memory we just break out,
			 * and bug the developer to introduce undo into his
			 * editor.
			 */
			if(!stralloc_catb(outsa, buf, ret))
				break;
		}
		close(fd);
	}

	do pid = waitpid(cpid, &pstat, 0);
	while(pid == -1 && errno == EINTR);

	sigaction(SIGINT, &intsave, NULL);
	sigaction(SIGQUIT, &quitsave, NULL);
	sigaction(SIGPIPE, &pipesave, NULL);
		sigprocmask(SIG_SETMASK, &omask, NULL);

		return pid == -1 ? -1 : pstat;
}
