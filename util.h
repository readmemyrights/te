#ifndef TE_UTIL_H
#define TE_UTIL_H

/* stddef.h and stralloc.h needs to be included */

int shellcmd(char *cmd, void *in, size_t insz, struct stralloc *outsa);

#endif
