#include <sys/wait.h>

#include "munit/munit.h"
#include "stralloc.h"
#include "util.h"

/* This whole tests depends on the shell and utilities working */
static MunitResult
test_shellcmd(const MunitParameter params[], void *userdata)
{
	struct stralloc sa;
	char input1[] = "DINOSAURS";
	int pstat;

	(void)params;
	(void)userdata;

	munit_assert((pstat = shellcmd("exit 100", NULL, 0, NULL)) != -1);
	munit_assert(WIFEXITED(pstat));
	munit_assert_int(WEXITSTATUS(pstat), ==, 100);

	munit_assert((pstat = shellcmd("kill -1 $$", NULL, 0, NULL)) != -1);
	munit_assert(WIFSIGNALED(pstat));
	munit_assert_int(WTERMSIG(pstat), ==, 1);

	munit_assert((pstat = shellcmd("grep -q 'dino'", input1, sizeof input1, NULL)));
	munit_assert(WIFEXITED(pstat));
munit_assert_int(WEXITSTATUS(pstat), !=, 0);

if(!stralloc_init(&sa, 16))
	return MUNIT_ERROR;

munit_assert((pstat = shellcmd("echo hello world", NULL, 0, &sa)) != -1);
munit_assert(WIFEXITED(pstat));
munit_assert_int(WEXITSTATUS(pstat), ==, 0);
munit_assert_memory_equal(12, sa.s, "hello world\n");

stralloc_zero(&sa);

munit_assert((pstat = shellcmd("awk '{ print $0 \" are cool\" }'", input1, sizeof input1, &sa)) != -1);
munit_assert(WIFEXITED(pstat));
munit_assert_int(WEXITSTATUS(pstat), ==, 0);
munit_assert_memory_equal(19, sa.s, "DINOSAURS are cool\n");

return MUNIT_OK;
}

static MunitTest util_tests[] = {
	{ "/shellcmd", test_shellcmd, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL },
	{ NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL }
};

MunitSuite util_testsuite = { "/util", util_tests, NULL, 1, MUNIT_SUITE_OPTION_NONE };
